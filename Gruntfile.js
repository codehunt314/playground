/*global require:false*/
'use strict';

/*global module:false*/
module.exports = function(grunt) {
	// Load grunt tasks automatically
	require('load-grunt-tasks')(grunt);

	// Time how long tasks take. Can help when optimizing build times
	require('time-grunt')(grunt);

	// Project configuration.
	grunt.initConfig({
		// Project settings
		yeoman: {
			// Configurable paths
			app: 'media',
			dist: 'dist/playground/static',
			templateRoot: 'sync/templates',
			templateDist: 'prod_templates'
		},

		// Metadata.
		pkg: grunt.file.readJSON('package.json'),
		banner: '/*! <%= pkg.title || pkg.name %> - v<%= pkg.version %> - ' + '<%= grunt.template.today("yyyy-mm-dd") %>\n' + '<%= pkg.homepage ? "* " + pkg.homepage + "\\n" : "" %>' + '* Copyright (c) <%= grunt.template.today("yyyy") %> <%= pkg.author.name %>;' + ' Licensed <%= _.pluck(pkg.licenses, "type").join(", ") %> */\n',

		// Empties folders to start fresh
		clean: {
			dist: {
				files: [{
					dot: true,
					src: [
						'.tmp',
						'<%= yeoman.dist %>/*',
						'!<%= yeoman.dist %>/.git*'
					]
				}, {
					dot: true,
					src: [
						'<%= yeoman.templateDist %>/*'
					]
				}]
			},
			minified: [
			]
		},

		// Reads HTML for usemin blocks to enable smart builds that automatically
		// concat, minify and revision files. Creates configurations in memory so
		// additional tasks can operate on them
		useminPrepare: {
			options: {
				dest: '<%= yeoman.dist %>'
			},
			html: '<% yeoman.templateRoot %>/new-sync.html'
		},

		less: {
			build: {
				options: {
					// yuicompress: true
				},
				files: {
					'<%= yeoman.app %>/css/home.css'	: '<%= yeoman.app %>/css/home.less',
					'<%= yeoman.app %>/css/style.css'	: '<%= yeoman.app %>/css/style.less',
				}
			}
		},

		cssmin: {
			add_banner: {
				options: {
					// relativeTo: '<%= yeoman.app %>/css/',
					// target: '<%= yeoman.dist %>/css/serai.min.css',
					banner: '<%= banner %>',
					keepSpecialComments: 0
				},

				files: [{
					dest: '<%= yeoman.dist %>/css/home.css',
					src: ['<%= yeoman.app %>/css/home.css']
				}, {
					dest: '<%= yeoman.dist %>/css/style.css',
					src: ['<%= yeoman.app %>/css/style.css']
				}]
			}
		},

		uglify: {
			dist: {
				files: {
					'<%= yeoman.dist %>/js/home-main.js': [
						'<%= yeoman.app %>/js/vendor/jquery-2.1.1.min.js', '<%= yeoman.app %>/js/main.js'
					],
					'<%= yeoman.dist %>/js/jszip.js': [
						'<%= yeoman.app %>/js/plugins/jszip.js'
					]
				}
			}
		},

		requirejs: {
			dist: {
				options: {
					almond: true,

					replaceRequireScript: [{
						files: ['<%= yeoman.templateDist %>/new-sync.html'],
						module: 'main',
						modulePath: '/js/main-built.min'
					}],

					// modules: [{name: 'main'}],

					baseUrl: '<%= yeoman.app %>/js/app',
					mainConfigFile: '<%= yeoman.app %>/js/app/config.js',
					generateSourceMaps: true,
					preserveLicenseComments: false,
					optimizeAllPluginResources: true,
					// include: ['main'],
					insertRequire: ['main'],
					optimize: 'uglify2',
					// name: '../vendor/almond', // assumes a production build using almond
					out: '<%= yeoman.dist %>/js/main-built.min.js'
				}
			}
		},

		copy: {
			dist: {
				files: [{
					expand: true,
					dot: true,
					cwd: '<%= yeoman.app %>',
					dest: '<%= yeoman.dist %>',
					src: [
						'sounds/{,*/}*.*',
						'img/{,*/}*.*',
						'fonts/Baskerville/{,*/}*.*',
						'fonts/serai-moments/fonts/{,*/}*.*',
						'fonts/font-awesome/fonts/{,*/}*.*',
						'fonts/ForecastFont/font/{,*/}*.*',
						'plugins/select2/*.png',
						'plugins/select2/*.gif',
						'html/featured_trip.json',
						'js/vendor/modernizr-2.6.2.min.js'
					]
				}]
			},
			template: {
				files: [{
					expand: true,
					dot: true,
					cwd: '<%= yeoman.templateRoot %>',
					dest: '<%= yeoman.templateDist %>',
					src: [
						'**'
					]
				}]
			}
		},

		rev: {
			dist: {
				files: {
					src: [
						'<%= yeoman.dist %>/js/{,*/}*.js',
						'<%= yeoman.dist %>/sounds/{,*/}*.*',
						'<%= yeoman.dist %>/js/{,*/}*.js.map',
						'<%= yeoman.dist %>/css/{,*/}*.css',
						'<%= yeoman.dist %>/img/{,*/}*.*',
						'<%= yeoman.dist %>/plugins/{,*/}*.*',
						'<%= yeoman.dist %>/fonts/Baskerville/{,*/}*.*',
						'<%= yeoman.dist %>/fonts/serai-moments/fonts/{,*/}*.*',
						'<%= yeoman.dist %>/fonts/font-awesome/fonts/{,*/}*.*',
						'<%= yeoman.dist %>/fonts/ForecastFont/font/{,*/}*.*'
					]
				}
			}
		},

		// Performs rewrites based on rev and the useminPrepare configuration
		// https://github.com/yeoman/grunt-usemin/issues/234
		usemin: {
			options: {
				assetsDirs: ['dist', 'dist/playground', 'dist/playground/static', 'dist/playground/static/img', 'dist/playground/static/js', 'dist/playground/static/fonts', 'dist/playground/static/sounds', 'dist/playground/static/sounds/xyz'],
				// assetsDirs: ['dist'],
				// basedir: ['<%= yeoman.dist %>'],
				patterns: {
					js: [
						[/(sounds\/.*?\.(?:wav))/gm, 'Update the JS to reference our revved sound'],
						[/(img\/.*?\.(?:gif|jpeg|jpg|png|webp|svg))/gm, 'Update the JS to reference our revved images'],
						[/(main\-built\.min\.js\.map)/gm, 'Update the JS to reference our revved source map'],
						[/(main\-built\-mobile\.min\.js\.map)/gm, 'Update the JS to reference our revved source map'],
					]
				},
				// basedir: ['dist/static']
			},
			html: [
				'<%= yeoman.templateDist %>/new-sync.html',
				'<%= yeoman.templateDist %>/home.html'],
			css: ['<%= yeoman.dist %>/css/{,*/}*.css'],
			js: ['<%= yeoman.dist %>/js/{,*/}*.js']
		},

		cdn: {
			options: {
				/** @required - root URL of your CDN (may contains sub-paths as shown below) */
				cdn: 'http://d2s0t4df3ulixm.cloudfront.net/playground/',
				/** @optional  - if provided both absolute and relative paths will be converted */
				flatten: false,
				/** @optional  - if provided will be added to the default supporting types */
				// supportedTypes: { 'phtml': 'html' }
			},
			dist: {
				/** @required  - string (or array of) including grunt glob variables */
				src: ['<%= yeoman.templateDist %>/new-sync.html',
					'<%= yeoman.templateDist %>/home.html'],
				/** @optional  - if provided a copy will be stored without modifying original file */
				// dest: './dist/static/'
			}
		},

		aws: grunt.file.readJSON('grunt-aws.json'),
		// 's3-sync': {
		// 	options: {
		// 		key: '<%= aws.key %>',
		// 		secret: '<%= aws.secret %>',
		// 		bucket: '<%= aws.bucket %>',
		// 		access: 'public-read',
		// 		concurrency: 10,
		// 		db : function(){
		// 			var level = require('level');
		// 			return level('./mydb');
		// 		}
		// 	},
		// 	production: {
		// 		files: [{
		// 			root: 'dist',
		// 			src: ['dist/**', '!dist/**/*.jpg', '!dist/**/*.png', '!dist/**/*.jpeg', '!dist/**/*.gif', '!dist/**/*.webp', '!dist/**/*.woff', '!dist/**/*.wav'], // Don't compress images!
		// 			dest: '/static/',
		// 			gzip: true,
		// 			compressionLevel: 9 // Max compression
		// 		},{
		// 			root: 'dist',
		// 			src: ['dist/**/*.jpg', 'dist/**/*.png', 'dist/**/*.jpeg', 'dist/**/*.gif', 'dist/**/*.webp', 'dist/**/*.woff', 'dist/**/*.wav'],
		// 			dest: '/static/'
		// 		}]
		// 	}
		// },


		s3: {
			options: {
				key: '<%= aws.key %>',
				secret: '<%= aws.secret %>',
				bucket: '<%= aws.bucket %>',
				access: 'public-read',
				region: 'ap-southeast-1',
				headers: {
					'Access-Control-Allow-Origin' : '*',
					'Cache-Control': 'max-age=630720000, public',
					'Expires': new Date(Date.now() + 63072000000).toUTCString()
				}
			},
			prod: {
				// These options override the defaults
				options: {
					maxOperations: 10
				},
				// Files to be uploaded.
				sync: [{
					src: 'dist/static/**/*.*',
					dest: 'playground/static/',
					rel: 'dist/static',
					options: {
						gzip: true,
						gzipExclude: ['.jpg', '.jpeg', '.png', '.gif', '.woff', '.wav', '.webp']
					}
				}]
			}

		}

	});

	// These plugins provide necessary tasks.
	// grunt.loadNpmTasks('grunt-contrib-concat');
	// grunt.loadNpmTasks('grunt-contrib-uglify');
	// grunt.loadNpmTasks('grunt-contrib-nodeunit');
	// grunt.loadNpmTasks('grunt-contrib-jshint');
	// grunt.loadNpmTasks('grunt-contrib-watch');

	// grunt.loadNpmTasks('grunt-contrib-cssmin');
	grunt.loadNpmTasks('grunt-requirejs');

	// Default task.
	// grunt.registerTask('default', ['jshint', 'nodeunit', 'concat', 'uglify']);
	grunt.registerTask('default', [
		'clean:dist',
		'less:build',
		'cssmin',
		'uglify',
		'copy:dist',
		'copy:template',
		'requirejs:dist',
		'useminPrepare',
		'rev',
		'usemin',
		// 'cdn',
		// 'clean:minified',
		// 's3:prod'
	]);
};
