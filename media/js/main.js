/*global $:false*/
'use strict';



var $content = $('.header .content'),
	$blur = $('.header .overlay'),
	wHeight = $(window).height();

$('.scroll-down').click(function(e){
	e.preventDefault();
	$('html, body').animate({scrollTop: wHeight});
});
$('.services-list').find('a').click(function(e){
	e.preventDefault();
	$(this).toggleClass('selected');
	if($('.services-list').find('a.selected').length > 0){
		$('.service-button').removeClass('disabled');
	}else{
		$('.service-button').addClass('disabled');
	}
});
$('.choose-service ').click(function(e){
	e.preventDefault();
	$('body').addClass('lightbox');
});
$('.modal .close').click(function(e){
	e.preventDefault();
	$('body').removeClass('lightbox');
});

$('.service-button').click(function(e){
	e.preventDefault();
	if($(this).hasClass('disabled')){
		return;
	}else{
		var services = $('.services-list').find('a.selected').map(function(){
			return $(this).attr('rel');
		}).toArray();
		$(this).next().removeClass('hide');
		setTimeout(function(){
			$('body').removeClass('lightbox');
		}, 500);
		$.ajax({
			method: 'post',
			url : '/playground/feedback/services/',
			data : {
				'services'	: services.join(','),
				'email'		: $('#user-email').val()
			}
		});
	}
});

$('.feedback-button').click(function(e){
	e.preventDefault();
	if($('#txt-feedback').val().trim().length > 0){
		$.ajax({
			method: 'post',
			url : '/playground/feedback/message/',
			data : {
				'feedback'		: $('#txt-feedback').val().trim()
			}
		});
		$(this).next().removeClass('hide');
	}
});

$(window).on('resize', function() {
	wHeight = $(window).height();
});

window.requestAnimFrame = (function() {
	return window.requestAnimationFrame ||
		window.webkitRequestAnimationFrame ||
		window.mozRequestAnimationFrame ||
		function(callback) {
			window.setTimeout(callback, 1000 / 60);
		};
})();

function Scroller() {
	this.latestKnownScrollY = 0;
	this.ticking = false;
}

Scroller.prototype = {
	init: function() {
		window.addEventListener('scroll', this.onScroll.bind(this), false);
	},

	onScroll: function() {
		this.latestKnownScrollY = window.scrollY;
		this.requestTick();
	},

	requestTick: function() {
		if (!this.ticking) {
			window.requestAnimFrame(this.update.bind(this));
		}
		this.ticking = true;
	},

	update: function() {
		var currentScrollY = this.latestKnownScrollY;
		this.ticking = false;

		var slowScroll = currentScrollY / 4,
			blurScroll = currentScrollY * 2;

		$content.css({
			'transform': 'translateY(+' + slowScroll + 'px)',
			'-moz-transform': 'translateY(+' + slowScroll + 'px)',
			'-webkit-transform': 'translateY(+' + slowScroll + 'px)'
		});

		$blur.css({
			'opacity': blurScroll / wHeight
		});
	}
};

/**
 * Attach!
 */
var scroller = new Scroller();
scroller.init();