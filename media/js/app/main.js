/*global define:false, TRIP_CLUSTER_JSON:false, USER_ID:false*/
define(function(require) {
	'use strict';

	var $ = require('jquery'),
		Backbone = require('backbone'),
		app = require('app'),
		Trips = require('collections/trips'),
		TripIndexView = require('views/trip-list'),
		TripDetailView = require('views/trip-detail');

	require('plugins/jquery.cluster');


	var Router = Backbone.Router.extend({
		routes: {
			'': 'home',
			'explore/' : 'home',
			'trip/:tripId/': 'tripPage'
		},
		initialize: function(options){
			this.tripCollection = options.tripCollection;
		},
		home: function() {
			if(this.lastDetailView){
				this.lastDetailView.remove();
				$('<div />', {
					class : 'trip-detail'
				}).insertAfter($('.trip-index'));
				delete this.lastDetailView;
			}
			if(this.tripIndexView){
				this.tripIndexView.$el.show();
			}else{
				this.tripIndexView = new TripIndexView({
					collection: this.tripCollection
				});
				this.tripIndexView.render();
			}
		},
		tripPage: function(tripId) {
			this.lastDetailView = new TripDetailView({
				tripId : tripId,
				listView: this,
				el : $('.trip-detail')
			}).render();
		}
	});

	var photos = [],page_counter=0,
		flickr_api = FLICKR_API_KEY,//'00695563a848a9c1d7e991660f2fb918',
		flickr_url = 'https://api.flickr.com/services/rest/?&api_key='+flickr_api+'&format=json&jsoncallback=?',
		flickr_method_findByUsername = flickr_url+ '&method=flickr.people.findByUsername&username=',
		flickr_method_UserInfo = flickr_url+'&method=flickr.people.getInfo&user_id=',
		flickr_method_getPhotos;
	if(AUTH){
		flickr_method_getPhotos = flickr_url+'&oauth_consumer_key='+flickr_api+'&oauth_token='+AUTH.oauth_token+'&method=flickr.people.getPhotos&per_page=500&content_type=7&extras=date_taken,description,geo,tags,machine_tags,views,url_n,url_m,url_s,url_l,url_o,url_sq,url_z,o_dims&page=';
	}else{
		flickr_method_getPhotos = flickr_url+'&oauth_consumer_key='+flickr_api+'&method=flickr.people.getPhotos&per_page=500&content_type=7&extras=date_taken,description,geo,tags,machine_tags,views,url_n,url_m,url_s,url_l,url_o,url_sq,url_z,o_dims&page=';
	}

	var initialTime = 0, totalPages = 0, estimateTime = -1,backgroundTimer;

	var sendRequest = function(callback) {
		$('.import-process').removeClass('hide');


		/*$.ajax({
			url: window.location.pathname + '?fu=' + USER_ID,
			dataType: 'json'
		}).done(function(data) {
			callback(data);
		}).fail(function() {
			setTimeout(function(){
				sendRequest(callback);
			}, 10*1000);
		});*/
		
		// backgroundTimer = setInterval(updateBackground , Math.random()*1000);

		// backgroundTimer = setInterval(loader , 100);

		// initialTime = new Date().getTime();
		photos = [],page_counter = 0;
		totalPages = Math.ceil(USER.totalphotos/500);
		for (var i=1;i<=totalPages; i++) {
			fetchUserPhotos(i);
		};
	};

	var fetchUserPhotos = function(page_num){
		$.ajax({
			url:flickr_method_getPhotos + page_num+'&user_id='+USER_ID,
			dataType:'json',
				xhrFields: {
			  withCredentials: true
			},
			crossDomain: true
		}).done(function(response){
			fetchPhotosCallBack(response)
		})
		// $.getJSON(flickr_method_getPhotos + page_num+'&user_id='+USER_ID, fetchPhotosCallBack);
	};


	var fetchPhotosCallBack = function (data){
		var index = -1;
		data.photos.photo.forEach(function(el){
					if(el.datetaken!=''){
						index = -1;
						for(var m=0; m<photos.length; m++){
							if(Date.parse(el.datetaken) > Date.parse(photos[m].datetaken)){
								index = m;
								break;
							}
						}
						if(index != -1){
							photos.splice(m, 0, el);
						}else{
							photos.push(el);
						}
					}
				})
		page_counter++;
		
		updateTimer(photos);

		
	};
	var newTime = 0,html = '';


	var updateTimer = function(photos){

		/*var t = new Date().getTime();
		newTime = (t - initialTime)*(totalPages) /  (page_counter*60);

		initialTime = t;

		if(newTime < estimateTime || estimateTime==-1){
			estimateTime = newTime;
		}

		if(estimateTime < 60){
			if(estimateTime<10){
				html = 'few Seconds ...';
			}else{
				html = Math.ceil(estimateTime/60*60)+' Seconds ...';
			}
		}else if(estimateTime > 60 && estimateTime < 60*60){
			html = Math.ceil(estimateTime/60) + ' Mins ...';
		}else{
			html = 'few Hours ...'
		}
		$('.time','.import-process').html(html);*/


		loader(360*page_counter/totalPages);

		if(totalPages == page_counter){
			
			$('.trip-list').cluster({
				photos:photos,
			
				callback:tripData,
			});
			
		}
	};	
	var loader = function(degree){
		if(degree<=180){
			setAngle('.top-outer-circle', (degree-135));
		}else{
			setAngle('.top-outer-circle', 45);
			setAngle('.bottom-outer-circle', (degree-315));

		}
		if(degree == 360){
			$('.trip-index').addClass('loaded');
		}
	};
	var setAngle = function(eleClass , angle){
		$(eleClass).css({ 'WebkitTransform': 'rotate(' + angle + 'deg)'});
    	// For Mozilla browser: e.g. Firefox
        $(eleClass).css({ '-moz-transform': 'rotate(' + angle + 'deg)'});
	};

	var tripData = function(data){
		data.user_id = USER_ID;

		var zip = new JSZip();
		zip.file("data.txt", JSON.stringify(data));
		var content = zip.generate({type : "blob", 'compression': 'DEFLATE'});
		var form = new FormData();
		form.append('name','nitin');
		form.append("file", content, 'xyz.zip');
		$.ajax({
			type:'POST',
			processData: false,
			contentType: false,
			url: '/playground/savetrip/',
			data: form
		});



		showTrips(data.trips);
	};

	var showTrips = function(data) {
		var tripCollection = new Trips(data);
		app.router = new Router({tripCollection: tripCollection});
		Backbone.history.start({
			pushState: true,
			root: app.root
		});
	};

	var randomMessages = [
			'Fetching Photos',
			'Fetching more photos',
			'Reading photos meta information',
			'Caching Photos information',
			'Analyzing meta information',
			'Clustring photos',
			'Reading location information',
			'Analyzing location information',
			'Forming Moments',
			'Forming Moments Cluster',
			'Analyzing Moments Cluster',
			'Forming Trip Journals',
			'Merging Trip Journals',
			'Cleaning up Cache'
		],
		counter = 0,
		timeout, delay;

	if (typeof TRIP_CLUSTER_JSON !== 'undefined') {
		showTrips(TRIP_CLUSTER_JSON);
		$('.trip-index').addClass('loaded');
	} else {
		var showMessage = function() {
			$('.random-message').text(randomMessages[counter++]);
			counter = counter > randomMessages.length - 1 ? 1 : counter;

			delay = Math.floor(Math.random() * 3000 + 2000);
			timeout = setTimeout(showMessage, delay);
		};
		showMessage();
		sendRequest(function(data) {
			showTrips(data);
		});
	}

	
	// $('.container').html('New Hello World' + TripCollection.length);
});





