/*global require:false*/
// Set the require.js configuration for your application.
require.config({

  // Initialize the application with the main application file.
  deps: ['main'],
  baseUrl: '/playground/site_media/js/app',
  paths: {
    // 'main' : '../main-built.min',

    // JavaScript folders.
    'plugins'                : '../plugins',

    // Libraries
    'jquery'                 : '../vendor/jquery-2.1.1.min',
    'underscore'             : '../vendor/underscore',
    'backbone'               : '../vendor/backbone',

    'backbone-relational'    : '../vendor/backbone-relational',
    'backbone-package'       : '../vendor/backbone-packages',
    'moment'                 : '../vendor/moment.min',

    // Plugins
    'text'                   : '../vendor/require-text-plugin',

    'templates'              : 'templates',
    'html'                   : '../html',
    'tooltipster'            : '../../plugins/jquery.tooltipster/jquery.tooltipster'

    // 'mediaRoot'           : '/static'
    // 'basePath' : '../../../.'
  },

  shim: {

    jquery: {
      exports: 'jQuery'
    },

    underscore: {
      exports: '_'
    },

    backbone: {
      deps: [ 'underscore', 'jquery' ],
      exports: 'Backbone'
    },

    'backbone-relational': ['backbone'],

    'plugins/jquery.inplaceEdit' : {
      deps: [ 'jquery' ],
    },

    'plugins/location-suggest' : {
      deps: [ 'jquery' ],
    },

    'plugins/moment-type.v2' : {
      deps: [ 'jquery' ],
    },

    'plugins/jquery.dragselect' : {
      deps: [ 'jquery' ],
    },

    'plugins/jquery.cluster' : {
      deps: [ 'jquery' ],
    },

    'plugins/jquery.justified.images' : {
      deps: [ 'jquery' ],
    },
    'plugins/jquery.sticky-kit' : {
      deps: [ 'jquery' ],
    },
    'plugins/jquery.lazyload.min': {
      deps: [ 'jquery' ],
    },
    'tooltipster': {
      deps: ['jquery']
    }
  }

});
