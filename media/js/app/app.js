/*jshint curly: false, multistr:true */
/*global define:false*/

define(function (require) {
	'use strict';
	var _ = require('underscore'),
		Backbone = require('backbone');

	var app = {
		root: '/playground/'
	};
	return _.extend(app, {}, Backbone.Events);
});