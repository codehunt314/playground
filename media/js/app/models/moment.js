/*global define:false */
define(function (require) {
	'use strict';
	var Backbone = require('backbone-package');

	require('models/photo');

	return Backbone.RelationalModel.extend({
		// id: '_id',
		defaults: function(){
			return {
				'deleted' : false
			};
		},
		validate: function(attrs, options) {
			var errors = [];
			if (!attrs.title || (attrs.title.trim().length === 0) ) {
				errors.push({
					message : 'Moment title missing',
					model : this,
					type : 'title'
				});
			}
			if( !attrs.types || attrs.types.length === 0){
				errors.push({
					message : 'Moment type not selected',
					model : this,
					type : 'type'
				});
			}
			return errors;
		},
		initialize: function(){
			Backbone.RelationalModel.prototype.initialize.call(this, arguments);
		},
		toJSON: function(){
			var obj = Backbone.RelationalModel.prototype.toJSON.call(this);
			obj.numPhotos = this.get('photos').length;
			obj.cid = this.cid;
			return obj;
		},
		relations: [{
			type: Backbone.HasMany,
			key: 'photos',
			relatedModel: require('models/photo'),
			collectionType : require('collections/photos'),
			reverseRelation: {
				key: 'moment',
				includeInJSON: 'id'
			}
		}, {
			type: Backbone.HasOne,
			key: 'cover-pic',
			relatedModel: require('models/photo'),
			reverseRelation: {
				type: Backbone.HasOne,
				key: 'moment-cover',
				includeInJSON: false
			}
		}]
	});
});