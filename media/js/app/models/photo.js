/*global define:false */
define(function (require) {
	'use strict';
	var Backbone = require('backbone-package');

	return Backbone.RelationalModel.extend({
		defaults: {
			order: 0,
			deleted : false
		},
		initialize: function(){
			Backbone.RelationalModel.prototype.initialize.call(this, arguments);
		}
	});
});