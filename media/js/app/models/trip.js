/*global define:false, moment:false */
define(function(require) {
	'use strict';
	var Backbone = require('backbone-package');
	require('moment');
	/**
	 * Function help to show duration in humane fashion
	 * @param  {date} fromDate
	 * @param  {date} toDate
	 * @return {string}          for two dates output could be "1st-5th Jan'2010, 1st Mar - 4th Apr, 2013, 30th Dec 2013 - 4th Jan 2014"
	 */
	function humanizeTimeDuration(fromDate, toDate, seperator) {
		var m1 = moment(fromDate),
			m2 = moment(toDate),
			d1 = m1.format('Do MMM YYYY').split(' '),
			d2 = m2.format('Do MMM YYYY').split(' '),
			sep = seperator || ' - ';
		// Year is same
		if (d2[2] === d1[2]) {
			// month is same
			if (d2[1] == d1[1]) {
				if (d2[0] == d1[0]) {
					return d1.join(' ');
				} else {
					return d1[0] + sep + d2[0] + ' ' + d1[1] + '&rsquo;' + d1[2];
				}
			} else {
				return d1[0] + ' ' + d1[1] + sep + d2[0] + ' ' + d2[1] + ', ' + d1[2];
			}
		} else {
			return d1.join(' ') + sep + d2.join(' ');
		}
	}

	require('models/moment');

	return Backbone.RelationalModel.extend({
		// id: '_id',
		initialize: function() {
			Backbone.RelationalModel.prototype.initialize.call(this, arguments);
		},
		/*defaults: function(){
			return {
				'title' : 'Title of the Trip',
				'about' : 'About this trip (optional)'
			};
		},*/
		validate: function(attrs, options) {
			if ( !attrs.title || (attrs.title.trim().length === 0) ) {
				return {
					message : 'Title of the Trip missing',
					model : this,
					type : 'title'
				};
			}
		},
		numPhotos: function() {
			return this.get('moments').reduce(function(count, moment) {
				return count + moment.get('photos').length;
			}, 0);
		},
		totalPhotos:function(){
			var photos=[];
			_.each( this.get('moments').models ,function(moment) { 
					_.each(moment.get('photos').models,function(photo){
															photos.push(photo.toJSON())
														} ) 
				});
			return photos;
		},
		toJSON: function() {
			var obj = Backbone.RelationalModel.prototype.toJSON.call(this);
			obj.numPhotos = this.numPhotos();
			obj.cid = this.cid;
			obj.humanizeDuration = humanizeTimeDuration(this.get('start_date'), this.get('end_date'));
			return obj;
		},
		jsonToServer: function(){
			var ret = this.pick('title', 'about', 'id');
			if(this.has('cover-pic')){
				ret['cover-pic'] = this.get('cover-pic').id;
			}
			ret.moments = this.get('moments').jsonToServer();
			return ret;
		},
		photoMarkDelete: function(photoId, flag){
			var moment, photo;
			for (var i = 0; i < this.get('moments').length; i++) {
				moment = this.get('moments').at(i);
				for (var j = 0; j < moment.get('photos').length; j++) {
					photo = moment.get('photos').at(j);
					if(photo.id === photoId){
						photo.set('deleted', flag);
						return;
					}
				}
			}
		},
		relations: [{
			type: Backbone.HasMany,
			key: 'moments',
			relatedModel: require('models/moment'),
			collectionType: require('collections/moments'),
			reverseRelation: {
				key: 'trip',
				includeInJSON: 'id'
			}
		}, {
			type: Backbone.HasOne,
			key: 'cover-pic',
			relatedModel: require('models/photo'),
			reverseRelation: {
				type: Backbone.HasOne,
				key: 'trip-cover',
				includeInJSON: false
			}
		}]
	});
});