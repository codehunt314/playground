/*jshint curly: false, multistr:true */
/*global define:false, moment:false */
define(function(require) {
	'use strict';

	var $ = require('jquery'),
		_ = require('underscore'),
		app = require('app'),
		Backbone = require('backbone-package'),
		TripModel = require('models/trip'),
		MomentModel = require('models/moment'),
		PhotoModel = require('models/photo'),
		PhotoPreview = require('views/photo-preview'),
		CreateMoment = require('views/create-moment'),
		MomentTypes = JSON.parse(require('text!html/moment-types.json')),
		TripPreviewView = require('views/trip-preview');

	require('text!templates/trip-detail.html');
	require('text!templates/moment-block.html');
	require('plugins/jquery.inplaceEdit');
	require('plugins/jquery.justified.images');
	require('plugins/location-suggest');
	require('plugins/moment-type.v2');
	require('plugins/jquery.dragselect');
	require('plugins/jquery.sticky-kit');
	require('plugins/jquery.lazyload.min');
	require('tooltipster');
	require('moment');

	var scrollTo = function(ele) {
		var $el = $(ele);
		$('html, body').animate({
			scrollTop: $el.offset().top - ($(window).height() / 2)
		});
	};

	var MomentView = Backbone.View.extend({
		className: 'moment-container',
		photoTemplate: _.template('<div order="<%=photo.order%>" class="photo-container" id="photo-<%=photo.id%>" style="height: <%=photo.displayHeight%>px;margin-right:<%=photo.marginRight%>px;"> \
			<div title="View fullscreen" class="tooltip photo-zoom image-button"><i class="fa fa-expand"></i></div>\
			<div title="Split this Moment from photos here onward" class="tooltip split-moment image-button"><i class="fa fa-code"></i></div>\
			<div title="Undo Delete" class="undo-button image-button tooltip"><i class="fa fa-undo"></i></div>\
			<img alt="<%=photo.title%>" class="image-thumb lazy" data-original="<%=photo.src%>" style="width:<%=photo.displayWidth%>px;height:<%=photo.displayHeight%>px;">\
		</div>'),
		chooseCoverTemplate: _.template(require('text!templates/choose-cover.html')),
		template: _.template(require('text!templates/moment-block.html')),
		events: {
			'click .btn-delete-moment'        : 'deleteMoment',
			'click .undo-moment-delete'       : 'undoDeleteMoment',
			'click .btn-reorder-photos'       : 'reorderImages',
			'mousedown .split-moment'         : 'splitMoment',
			'click .change-moment-cover'      : 'changeCoverClicked',
			'click .choose-cover-image-thumb' : 'toggleCoverSelected',
			'click .choose-cover-done'        : 'chooseCoverDone'
		},
		initialize: function(options) {
			this.tripView = options.tripView;
			this.listenTo(this.model, 'change:title', this.focusTitle);
			this.listenTo(this.model, 'change:types', this.focusType);
			this.listenTo(this.model, 'change:cover-pic', this.changeCover);
			this.listenTo(this.model, 'remove:photos', _.debounce(this.photoRemoved, this));
			this.listenTo(this.model, 'add:photos', _.debounce(this.photoAdded, this));
			this.listenTo(this.model.get('photos'), 'change:order', _.debounce(this.photosReordered, this));
		},
		serialize: function() {
			return {
				moment: this.model.toJSON()
			};
		},
		render: function() {
			this.$el.html(this.template(this.serialize()));
			this.makeEditable();
			this.$('.photos-container').justifiedImages({
				images: this.model.get('photos').toJSON(),
				thumbnailPath: function(photo, width, height) {
					var purl = photo.url_s;
					if (photo.url_n && (width > photo.width_s * 1.1 || height > photo.height_s * 1.1)) purl = photo.url_n;
					if (photo.url_m && (width > photo.width_n * 1.1 || height > photo.height_n * 1.1)) purl = photo.url_m;
					if (photo.url_z && (width > photo.width_m * 1.1 || height > photo.height_m * 1.1)) purl = photo.url_z;
					if (photo.url_l && (width > photo.width_z * 1.1 || height > photo.height_z * 1.1)) purl = photo.url_l;
					return purl;
				},
				getSize: function(photo) {
					return {
						width: photo.width_s,
						height: photo.height_s
					};
				},
				template: this.photoTemplate,
				dataObject: 'photo',
				imageSelector: 'image-thumb',
				imageContainer: 'photo-container',
				margin: 1
			});
			this.$('.buttons-in-header').stick_in_parent({
				offset_top: 60
			});
			this.checkImageSplitter();

			$('img.image-thumb', '#moment-block-' + this.model.cid).lazyload();

			this.$('.tooltip.btn').tooltipster({
				position: 'right'
			});
			this.$('.tooltip.undo-moment-delete').tooltipster({
				position: 'left'
			});
			this.$('.tooltip.image-button').tooltipster();
			return this;
		},
		checkImageSplitter: function() {
			var lastPhoto;
			this.model.get('photos').each(function(photo) {
				if (lastPhoto) {
					var m_last = moment(lastPhoto.get('datetaken')).unix(),
						m_current = moment(photo.get('datetaken')).unix();
					if (Math.abs(m_last - m_current) > 3600 / 12) {
						this.$('#photo-' + photo.id).find('.split-moment').addClass('active');
					}
				}
				lastPhoto = photo;
			}, this);
		},
		splitMoment: function(e) {
			e.preventDefault();
			e.stopPropagation();
			var id = $(e.target).parents('.photo-container').attr('id').split('-')[1];
			var newMoment = new MomentModel(this.model.pick('day_num')),
				photo = PhotoModel.findOrCreate({
					id: id
				}, {
					create: false
				}),
				index = this.model.get('photos').indexOf(photo);
			_.each(this.model.get('photos').rest(index), function(photo) {
				photo.set('moment', newMoment);
			});
			this.model.collection.add(newMoment, {
				at: this.model.collection.indexOf(this.model) + 1
			});
			// newMoment.set('trip', this.tripView.model);

			// this.tripView.dragndrop.dragnselect('refreshEvent');
			this.tripView.checkForHeaderOptions();

		},
		renderPhotos: function() {
			this.$('.photos-container').justifiedImages('refresh', this.model.get('photos').toJSON());
			// this.tripView.dragndrop.dragnselect('refreshEvent');
			this.tripView.checkForHeaderOptions();
			$('img.image-thumb', '#moment-block-' + this.model.cid).lazyload();
		},
		photoRemoved: function() {
			console.log('removed', arguments);
			this.render();
		},
		photoAdded: function() {
			console.log('added', arguments);
			this.render();
		},
		photosReordered: function() {
			console.log('reordered', arguments);
			this.model.get('photos').sort();
			this.renderPhotos();
		},
		makeEditable: function() {
			var self = this;
			this.$('.editable').each(function(i, ele) {
				$(ele).neatEdit({
					'type': 'textbox',
					placeholder: $(ele).attr('placeholder'),
					'saveOnEnter': false,
					'callback': _.bind(self.valueChanged, self)
				});
			});
			this.$('.location-suggest').locationSuggest({
				placeholder: 'Where did it happened ?',
				type: '',
				url: '/playground/locsearch/',
				callback: _.bind(this.momentLocationChanged, this)
				// initValue : 'Some'
			});
			this.$('.moment-type').momentType({
				momentTypes: MomentTypes,
				selectedMomentTypes: this.model.get('types'),
				callback: _.bind(this.momentTypesChanged, this),
				maxSelection: 1
			});
		},
		momentLocationChanged: function(value) {
			this.model.set('location', value);
			this.tripView.validate();
		},
		valueChanged: function(value, $ele) {
			this.model.set($ele.attr('name'), value);
			this.tripView.validate();
		},
		momentTypesChanged: function(momentTypes) {
			this.model.set('types', momentTypes);
			this.tripView.validate();
		},
		focusTitle: function() {
			if (!this.model.has('title') || this.model.get('title') === '') {
				this.$('.moment-title').neatEdit('focus');
				scrollTo(this.$('.moment-title'));
			}
		},
		focusType: function() {
			if (!this.model.has('types') || this.model.get('types').length === 0) {
				this.$('.moment-type').momentType('focus');
				scrollTo(this.$('.moment-type'));
			}
		},
		deleteMoment: function(e) {
			e.preventDefault();
			this.$('.moment-block').addClass('hide');
			this.$('.moment-deleted').removeClass('hide');
			this.model.set('deleted', true);
			this.$('.selected').removeClass('selected');
			this.tripView.validate();
		},
		undoDeleteMoment: function(e) {
			e.preventDefault();
			this.$('.moment-deleted').addClass('hide');
			this.$('.moment-block').removeClass('hide');
			this.model.set('deleted', false);
			this.tripView.validate();
		},
		reorderImages: function(e) {
			e.preventDefault();
			this.tripView.reorderMomentPictures(this.model);
		},
		changeCoverClicked: function(e) {
			e.preventDefault();
			e.stopPropagation();
			var html = this.chooseCoverTemplate({
				moment: this.model.toJSON(),
				cid: this.model.cid
			});

			this.tripView.showModal();
			$('.modal-inner', this.tripView.$el).html(html);

			var template = _.template('<img alt="<%=photo.title%>" id="choose-cover-photo-<%=photo.id%>" class="choose-cover-image-thumb" src="<%=photo.url_s%>" alt="" style="width:<%=photo.displayWidth%>px;height:<%=photo.displayHeight%>px;">');
			$('.images', this.tripView.$el).justifiedImages({
				images: this.model.get('photos').toJSON(),
				template: template, dataObject: 'photo',
				imageSelector: '.image-thumb',
				imageContainer: '.images-block',
				margin: 3
			});
			if (this.model.has('cover-pic')) {
				$('#choose-cover-photo-' + this.model.get('cover-pic').id).addClass('selected');
			}
			this.tripView.stopParentDragNDrop();
			this.tripView.addEventsForModal();
		},

		changeCover: function() {
			var photo = this.model.get('cover-pic');
			this.$('.moment-header').addClass('has-cover').css('background-image', 'url(' + photo.get('url_o') + ')');
		}
	});

	var TripView = Backbone.View.extend({
		template: _.template(require('text!templates/trip-detail.html')),
		chooseCoverTemplate: _.template(require('text!templates/choose-cover.html')),
		movePhotosTemplate: _.template(require('text!templates/move-photo.html')),
		orderPhotosTemplate: _.template(require('text!templates/reorder-photo.html')),
		// photoPreviewTemplate: _.template(require('text!templates/photo-preview.html')),
		events: {
			'click .home-link': 'goBack',
			'click .preview-publish': 'previewTripClicked',
			'click .error-item-link': 'focusErrorField',
			'click .right-section': 'clearSelection',
			'click .delete-image': 'deleteImage',
			'click .undo-button': 'undoToDelete',
			'click .create-moment': 'createMomentFunction',
			'mousedown .photo-zoom': 'previewImage',
			'click .move-image': 'movePhotos',
			'click .modal-close': 'closeModal',
			'click .modal-photos-container': 'selectMomentContainerAndMovePhotos',
			'click .reorder-btn': 'reorder',
			'click .move-done': 'doneWithMove',
			'click .reorder-done': 'reorderCompleted',
			'click .change-trip-cover': 'changeCoverClicked',
			'click .choose-cover-image-thumb': 'toggleCoverSelected',
			'click .choose-cover-done': 'chooseCoverDone'
		},
		initialize: function(options) {
			this.model = TripModel.findOrCreate({
				id: options.tripId
			}, {
				create: false
			});
			this.currentSelectedPhotos = [];
			this.dummyPhoto = $('<div class="dummyPhoto reorder-image-thumb"><div class="targetArea"></div><div class="photo-img"></div></div>');

			// this.listView = options.listView;
			this.listenTo(this.model, 'change:title', this.focusTitle);
			this.listenTo(this.model, 'add:moments', this.showMoment);
			this.photoPreview = '';

		},
		serialize: function() {
			return {
				trip: this.model.toJSON()
			};
		},
		render: function() {
			this.$el.html(this.template(this.serialize()));
			this.model.get('moments').each(function(moment) {
				this.showMoment(moment);
			}, this);
			this.makeEditable();
			/*this.$('.photos-container').justifiedGallery({
				rowHeight : 150,
				lastRow : 'nojustify',
				margins : 10
			});*/

			this.dragndrop = $(this.$el).dragnselect({
				'imageClass': '.photo-container',
				'selectedClassName': 'selected',
				'multipleDrag': false,
				'deletedClassName': 'deleted',
				'imageSelected': _.bind(this.imageSelected, this),
				// 'orderingStarted':_.bind(this.imageOrderingStarted),
				// 'callback':_.bind(this.callBackFromPlugin,this)
			});
			this.validate();

			this.$('.options').tooltipster({
				trigger: 'hover',
				animation: 'fade'
			});

			return this;
		},
		showMoment: function(moment) {
			var collection = this.model.get('moments'),
				currentIndex = collection.indexOf(moment),
				prevMoment, after;
			var view = new MomentView({
				model: moment,
				id: 'moment-block-' + moment.cid,
				tripView: this
			});
			if (currentIndex > 0) {
				prevMoment = collection.at(currentIndex - 1);
				after = this.$('#moment-block-' + prevMoment.cid);
				view.$el.insertAfter(after);
			} else {
				this.$('.moments-container').append(view.$el);
			}
			view.render();
			this.validate();
		},
		imageSelected: function(photo) {
			this.checkForHeaderOptions();
		},
		checkForHeaderOptions: function() {
			if ($('.photo-container.selected ').length > 0) {
				$('#header').addClass('active');
			} else {
				$('#header').removeClass('active');
			}

		},
		makeEditable: function() {
			var self = this;
			this.$('.editable').each(function(i, ele) {
				$(ele).neatEdit({
					'type': 'textbox',
					placeholder: $(ele).attr('placeholder'),
					'saveOnEnter': false,
					'callback': _.bind(self.valueChanged, self)
				});
			});

		},
		focusTitle: function() {
			if (!this.model.has('title') || this.model.get('title') === '') {
				this.$('.trip-title').neatEdit('focus');
				scrollTo(this.$('.trip-title'));
			}
		},
		valueChanged: function(value, $ele) {
			this.model.set($ele.attr('name'), value);
			this.validate();
		},
		goBack: function(e) {
			e.preventDefault();
			this.$el.hide();
			app.router.navigate('', {
				trigger: true,
				replace: false
			});
			// this.listView.$el.show();
		},
		previewTripClicked: function(e) {
			e.preventDefault();
			e.stopPropagation();
			if (this.errors.length) {
				this.$('.error-list').toggleClass('hide');
			} else {
				this.previewTrip();
			}
		},
		validate: function() {
			this.errors = [];
			if (!this.model.isValid()) {
				this.errors.push(this.model.validationError);
			}
			this.model.get('moments').each(function(moment) {
				if (!moment.get('deleted') && !moment.isValid()) {
					this.errors.push.apply(this.errors, moment.validationError);
				}
			}, this);
			if (this.errors.length) {
				this.$('.preview-publish').addClass('inactive');
				this.$('.error-bubble').text(this.errors.length);
				this.renderErrors();
			} else {
				this.$('.preview-publish').removeClass('inactive');
				this.$('.error-list').addClass('hide');
			}
		},
		renderErrors: function() {
			this.$('.error-count').text(this.errors.length);
			var ul = this.$('.error-list-ul');
			_.each(this.errors, function(error) {
				var id = ['error-li', error.type, error.model.cid].join('-');
				if (ul.children('li#' + id).length === 0) {
					$('<li />', {
						id: id,
						class: 'error-item',
						html: '<a href="#" class="error-item-link">' + error.message + '</a>'
					}).appendTo(ul);
				}
			}, this);
			_.each(ul.children('.error-item').toArray(), function(ele) {
				var errorObject = this.findErrorFromElement(ele);
				if (!errorObject) {
					$(ele).animate({
						height: 0
					}, 200, function() {
						$(this).remove();
					});
				}
			}, this);
		},
		focusErrorField: function(e) {
			e.preventDefault();
			var $el = $(e.target).hasClass('error-item') ? $(e.target) : $(e.target).parents('.error-item'),
				error = this.findErrorFromElement($el);
			if (error.type === 'title') {
				error.model.trigger('change:title'); //.set('title', '');
			}
			if (error.type === 'type') {
				error.model.trigger('change:types'); //.set('types', []);
			}
		},
		findErrorFromElement: function(ele) {
			var $el = $(ele),
				id = $el.attr('id'),
				type = id.split('-')[2],
				cid = id.split('-')[3],
				error;
			if (this.errors && this.errors.length) {
				for (var i = 0; i < this.errors.length; i++) {
					error = this.errors[i];
					if (error.type === type && error.model.cid === cid) {
						return error;
					}
				}
			}
		},
		previewImage: function(e) {
			e.preventDefault();
			e.stopPropagation();
			this.showModal();

			var photo_id = _.last($(e.currentTarget).parent('.photo-container').attr("id").split('-'))
			var photo = PhotoModel.findOrCreate({
				id: photo_id
			});

			this.photoPreview = new PhotoPreview({
				photo_id: photo_id,
			});

			$('.modal-inner', this.$el).html(this.photoPreview.$el);
			this.photoPreview.render();

		},
		deleteImage: function(e) {
			e.preventDefault();
			var self = this;
			$('.photo-container.selected ').each(function() {
				var id = _.last($(this).attr('id').split('-'));
				self.model.photoMarkDelete(id, true);
				$(this).removeClass('selected').addClass('deleted');
			});
			this.checkForHeaderOptions();
		},
		undoToDelete: function(e) {
			var id = _.last($(e.currentTarget).parent('.photo-container').attr('id').split('-'));
			this.model.photoMarkDelete(id, false);
			$(e.currentTarget).parent('.photo-container').removeClass('deleted');
		},
		clearSelection: function(e) {
			$('.photo-container.selected ').removeClass('selected');
			this.checkForHeaderOptions();
		},
		movePhotos: function(e) {
			this.showModal();
			this.selectedPic();

			$('.modal-inner', this.$el).html(this.movePhotosTemplate({
				'photos': this.currentSelectedPhotos,
				'moments': this.model.get('moments').toJSON()
			}));
			$('.all-moments').css({
				'height': parseInt($('.modal').css('height')) - 250 + 'px'
			});

			_.each($('.modal-selected-photo'), function(el) {
				$(el).css('width', $(el).attr('org-width') * 100 / $(el).attr('org-height'));
			});

			_.each($('.image-thumb', '.modal-photos-container'), function(el) {
				$(el).css('width', $(el).attr('org-width') * 50 / $(el).attr('org-height'));
			});
		},
		selectedPic: function() {
			this.currentSelectedPhotos = [];
			var photo;
			_.each($('.photo-container.selected'), function(el) {
				photo = PhotoModel.findOrCreate({
					id: _.last($(el).attr('id').split('-'))
				});
				this.currentSelectedPhotos.push(photo);
			}, this);
		},
		selectMomentContainerAndMovePhotos: function(e) {
			this.selectedMomentId = _.last($(e.currentTarget).attr('id').split('-'));
			$('.modal-photos-container').addClass('unselect');
			$(e.currentTarget).removeClass('unselect');

			$('.selected-pic-block').css('display', 'none');

			_.each(this.currentSelectedPhotos.reverse(), function(photo) {
				photo.set('moment', this.model.get('moments').get(this.selectedMomentId));
				$(e.currentTarget).prepend($('#modal-photo-' + photo.id));
			}, this);

		},
		reorderMomentPictures: function(moment) {
			this.showModal();
			$('.modal-inner', this.$el).html(this.orderPhotosTemplate({
				'moment': moment.toJSON()
			}));
			this.stopParentDragNDrop();
			this.addEventsForModal();
		},
		reorder: function(e) {
			$('.modal-inner', this.$el).html(this.orderPhotosTemplate({
				'moment': this.model.get('moments').get(this.selectedMomentId).toJSON()
			}));
			this.stopParentDragNDrop();
			this.addEventsForModal();
		},
		stopParentDragNDrop: function() {
			this.dragndrop.dragnselect('removeEvents');
		},
		closeModal: function(e) {
			if (e) {
				e.preventDefault();
				e.stopPropagation();
			}
			if (this.photoPreview) {
				this.photoPreview.remove();
			}
			this.dragndrop.dragnselect('addEvents');
			// $('.modal', this.$el).removeClass('show');
			$('body').removeClass('show-modal');
			this.checkForHeaderOptions();
		},
		showModal: function() {
			$('body').addClass('show-modal');
		},
		addEventsForModal: function(e) {
			$('.reorder-image-thumb').on('mousedown', _.bind(this.reorderImageMouseDown, this));
			$('.modal').on('mouseup', _.bind(this.modalMouseUp, this));
		},

		reorderImageMouseDown: function(e) {
			this.currentPhoto = $(e.currentTarget);

			$('.photo-img', this.dummyPhoto).html(this.currentPhoto.clone())
			$(this.dummyPhoto).insertAfter($(this.currentPhoto))
				.css({
					'left': $(this.currentPhoto).offset().left,
					'top': $(this.currentPhoto).offset().top - $(window).scrollTop()
				});
			this.currentMouseDifference = [$(this.currentPhoto).offset().left - e.pageX, $(this.currentPhoto).offset().top - e.pageY]
			$('.reorder-image-thumb').css('opacity', 1);
			$(this.currentPhoto).css('opacity', 0);

			$('.modal').on('mousemove.reorderPhotoManagement', this.reorderImageMove.bind(this));
		},
		modalMouseUp: function(e) {
			$(this.dummyPhoto).remove();
			$(this.currentPhoto).css('opacity', 1);
			$('.modal').off('mousemove.reorderPhotoManagement');
		},
		reorderImageMove: function(e) {
			var top = 0,
				left = 0;

			top = e.pageY + this.currentMouseDifference[1] - $(window).scrollTop(); //+ this.imagesParentContainer.scrollTop();
			left = e.pageX + this.currentMouseDifference[0];

			$(this.dummyPhoto).css({
				'left': left,
				'top': top,
			});

			this.currentMouseX = e.pageX;
			this.findIntersectors($('.targetArea', this.dummyPhoto), '.reorder-image-thumb');

		},
		findIntersectors: function(targetSelector, intersectorsSelector) {

			var intersectors = [];

			var $target = $(targetSelector);
			var tAxis = $target.offset();
			var t_x = [tAxis.left, tAxis.left + $target.outerWidth()];
			var t_y = [tAxis.top, tAxis.top + $target.outerHeight()];


			$(intersectorsSelector).each(_.bind(function(c, ele) {
				var $this = $(ele);
				var thisPos = $this.offset();
				var i_x = [thisPos.left, thisPos.left + $this.outerWidth()]
				var i_y = [thisPos.top, thisPos.top + $this.outerHeight()];

				if (t_x[0] < i_x[1] && t_x[1] > i_x[0] && t_y[0] < i_y[1] && t_y[1] > i_y[0]) {

					this.imageDroppingArea($this)

				}

			}, this));
			// return intersectors;
		},
		imageDroppingArea: function(ele) {

			if (!$(ele).hasClass('dummyPhoto') && $(ele) != this.currentPhoto) {

				if (this.currentMouseX < $(ele).offset().left + $(ele).width() / 2) {
					$(this.currentPhoto).insertBefore($(ele));
				} else {
					$(this.currentPhoto).insertAfter($(ele));
				}

			}

		},

		reorderCompleted: function(e) {
			var photo = '';
			_.each($('.reorder-image-thumb'), function(el, index) {
				photo = PhotoModel.findOrCreate({
					id: _.last($(el).attr('id').split('-'))
				});
				photo.set('order', index);
			}, this);

			this.closeModal(e);
		},

		doneWithMove: function(e) {
			this.closeModal(e);
		},

		createMomentFunction: function(e) {
			this.stopParentDragNDrop();
			this.showModal();

			this.selectedPic();

			this.createMoment = new CreateMoment({
				'tripModel': this.model,
				'photos': this.currentSelectedPhotos,
				'onCompleted': _.bind(this.creationCompleted, this)
			})

			$('.modal-inner', this.$el).html(this.createMoment.$el);
			this.createMoment.render();

		},
		creationCompleted: function(e) {
			this.clearAllSelection();
			this.closeModal(e);
		},


		clearAllSelection: function() {
			$('#header').removeClass('active');
			$('.selected').removeClass('selected');
		},

		changeCoverClicked: function(e) {
			e.preventDefault();
			var html = this.chooseCoverTemplate({
				moments: this.model.get('moments').toJSON()
			});
			this.showModal();
			$('.modal-inner', this.$el).html(html);
			if (this.model.has('cover-pic')) {
				$('#choose-cover-photo-' + this.model.get('cover-pic').id).addClass('selected');
			}

			var template = _.template('<img alt="<%=photo.title%>" id="choose-cover-photo-<%=photo.id%>" class="choose-cover-image-thumb" src="<%=photo.url_s%>" alt="" style="width:<%=photo.displayWidth%>px;height:<%=photo.displayHeight%>px;">');
			$('.images', this.$el).justifiedImages({
				images: this.model.totalPhotos(),
				template: template,
				dataObject: 'photo',
				imageSelector: '.image-thumb',
				imageContainer: '.images-block',
				margin: 3
			});
			this.stopParentDragNDrop();
			this.addEventsForModal();
		},

		toggleCoverSelected: function(e) {
			e.preventDefault();
			$('.choose-cover-image-thumb').removeClass('selected');
			$(e.target).toggleClass('selected');
		},

		chooseCoverDone: function(e) {
			e.preventDefault();
			var $selectedImage = $('.choose-cover-image-thumb.selected');
			if ($selectedImage.length) {
				var id = _.last($selectedImage.attr('id').split('-')),
					photo = PhotoModel.findOrCreate({
						id: id
					});
				if ($(e.target).hasClass('is-moment-cover')) {
					var cid = _.last($(e.target).attr('id').split('-')),
						moment = this.model.get('moments').get(cid);
					moment.set('cover-pic', photo);
				} else {
					this.model.set('cover-pic', photo);
					this.$('.trip-header').addClass('has-cover').css('background-image', 'url(' + photo.get('url_o') + ')');
				}
			}
			this.closeModal(e);
		},

		previewTrip: function() {
			var tripPreviewView = new TripPreviewView({
					model: this.model,
					tripView: this
				}),
				$el = $('.trip-preview');
			this.$el.hide();
			if ($el.children('.trip-preview-container').length) {
				$el.children('.trip-preview-container').data('view').remove();
			}
			$el.append(tripPreviewView.$el).show();
			tripPreviewView.render();
		}
	});
	return TripView;
});