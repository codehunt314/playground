/*jshint curly: false, multistr:true */
/*global define:false */
define(function (require) {
	'use strict';

	var $ = require('jquery'),
		_ = require('underscore'),
		app = require('app'),
		Backbone = require('backbone-package');

	require('text!templates/trips-index.html');

	return Backbone.View.extend({
		el: $('.trip-index'),
		template: _.template(require('text!templates/trips-index.html')),
		events : {
			'click .trip-item': 'loadTrip'
		},
		initialize: function(){
			this.listenTo(this.collection, 'change:cover-pic', this.coverChanged);
		},
		serialize: function(){
			return {data : this.collection.toJSON()};
		},
		coverChanged: function(trip, photo){
			$('#trip-item-'+trip.id).find('.trip-header-cover').css('background-image', 'url("'+photo.get('url_n')+'")');
			// console.log(arguments);
		},
		render: function(){
			this.$('.trip-list').html(this.template(this.serialize()));
		},
		loadTrip: function(e){
			e.preventDefault();
			this.$el.hide();

			var ele = $(e.currentTarget),
				id = _.last(ele.attr('id').split('-'));

			if($(e.currentTarget).hasClass('published')){
				window.location.href = window.location.origin + '/trip/' + id + '/';
			}else{
				app.router.navigate('trip/' + id + '/', {trigger:true, replace:false});
			}
		}
	});
});