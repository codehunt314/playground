/*jshint curly: false, multistr:true */
/*global define:false, moment:false */
define(function (require) {
	'use strict';

	var $ = require('jquery'),
		_ = require('underscore'),
		Backbone = require('backbone-package');

	require('text!templates/trip-detail-preview.html');
	require('text!templates/moment-block-preview.html');
	require('plugins/jquery.justified.images');
	require('moment');

	var MomentView = Backbone.View.extend({
		className : 'moment-container',
		photoTemplate : _.template('<div order="<%=photo.order%>" class="photo-container" id="photo-<%=photo.id%>" style="margin-right:<%=photo.marginRight%>px;"> \
			<img alt="<%=photo.title%>" class="image-thumb" src="<%=photo.src%>" style="width:<%=photo.displayWidth%>px;height:<%=photo.displayHeight%>px;">\
		</div>'),
		template: _.template(require('text!templates/moment-block-preview.html')),
		initialize: function(options){
			this.tripView = options.tripView;
		},
		serialize: function(){
			return { moment : this.model.toJSON() };
		},
		render: function(){
			this.$el.html(this.template(this.serialize()));
			this.$('.photos-container').justifiedImages({
				images : this.model.get('photos').jsonForPreview(),
				template : this.photoTemplate,
				dataObject : 'photo',
				imageSelector : 'image-thumb',
				imageContainer : 'photo-container',
				maxRows : 2,
				getSize: function(photo) {
					return {
						width: photo.width_s,
						height: photo.height_s
					};
				},
				thumbnailPath: function(photo, width, height) {
					var purl = photo.url_s;
					if (photo.url_n && (width > photo.width_s * 1.1 || height > photo.height_s * 1.1)) purl = photo.url_n;
					if (photo.url_m && (width > photo.width_n * 1.1 || height > photo.height_n * 1.1)) purl = photo.url_m;
					if (photo.url_z && (width > photo.width_m * 1.1 || height > photo.height_m * 1.1)) purl = photo.url_z;
					if (photo.url_l && (width > photo.width_z * 1.1 || height > photo.height_z * 1.1)) purl = photo.url_l;
					return purl;
				},
				margin: 1
			});
			return this;
		}
	});

	var TripView = Backbone.View.extend({
		className : 'trip-preview-container',
		template: _.template(require('text!templates/trip-detail-preview.html')),
		events: {
			'click .continue-editing' : 'continueEditing',
			'click .publish-now' : 'publishNow'
		},
		initialize: function(options){
			this.tripView = options.tripView;
			this.$el.data('view', this);
		},
		serialize: function(){
			return {trip : this.model.toJSON()};
		},
		render: function(){
			this.$el.html(this.template(this.serialize()));
			_.each(this.model.get('moments').where({deleted:false}), function(moment){
				this.showMoment(moment);
			}, this);
			return this;
		},
		showMoment: function(moment){
			var view = new MomentView({
				model : moment,
				id : 'moment-block-' + moment.cid,
				tripView : this
			});
			this.$('.moments-container').append(view.$el);
			view.render();
		},
		publishNow: function(e){
			e.preventDefault();
			console.log(this.model.jsonToServer());
			var form = $('<form />', {
				method : 'post',
				action : '/playground/savetriptoserai/'
			}).appendTo('body');
			$('<input type="hidden" name="data"/>').val(JSON.stringify(this.model.jsonToServer())).appendTo(form);
			form.submit();
		},
		continueEditing: function(e){
			e.preventDefault();
			this.remove();
			this.tripView.$el.show();
		}
	});
	return TripView;
});