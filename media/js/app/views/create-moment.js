/*jshint curly: false, multistr:true */
/*global define:false */
define(function (require) {
	'use strict';

	var $ = require('jquery'),
		_ = require('underscore'),
		Backbone = require('backbone-package'),
		MomentModel = require('models/moment'),
		PhotoModel = require('models/photo');

	require('text!templates/create-moment.html');

	var CreateMoment = Backbone.View.extend({
		className : 'moment-creation',
		template: _.template(require('text!templates/create-moment.html')),
		events : {
			'click .creation-done-btn' : 'changeMoment'
		},
		initialize: function(options){
			this.tripModel = options.tripModel;
			this.selectedPhotos = options.photos;
			this.onCompleted = options.onCompleted;

		},
		serialize: function(){
			return { 
				photos 		: 	this.selectedPhotos,
				moments  	: 	this.tripModel.get('moments').toJSON()
			}
		},
		render: function(){
			this.$el.html(this.template(this.serialize()));
			this.addEventsForModal();
			return this;
		},

		addEventsForModal:function(e){
			$('.new-moment-container').on('mousedown',_.bind(this.reorderMomentMouseDown,this));
			$('.modal').on('mouseup',_.bind(this.modalMouseUp,this));
		},

		reorderMomentMouseDown: function(e){
			this.currentMoment = $(e.currentTarget);

			this.dummyMoment = this.currentMoment.clone().addClass('dummyMoment');
			$(this.dummyMoment).insertAfter( $(this.currentMoment) )
							.css({
								'left':$(this.currentMoment).offset().left,
								'top':$(this.currentMoment).offset().top - $(window).scrollTop()
							});
			this.currentMouseDifference = [$(this.currentMoment).offset().left - e.pageX , $(this.currentMoment).offset().top - e.pageY]
			$(this.currentMoment).css('opacity',0);
			$('.modal').on('mousemove.reorderPhotoManagement', this.reorderMomentMove.bind(this));
		},

		modalMouseUp: function(e){
			$(this.dummyMoment).remove();
			$(this.currentMoment).css('opacity',1);
			$('.modal').off('mousemove.reorderPhotoManagement');
		},

		reorderMomentMove:function(e){
			var top = 0 ,
				left = 0;

			top = e.pageY + this.currentMouseDifference[1] - $(window).scrollTop() ;//+ this.imagesParentContainer.scrollTop();
			left = e.pageX + this.currentMouseDifference[0];

			$(this.dummyMoment).css({
				'left':left,
				'top':top,
			});

			this.currentMouseX = e.pageX;
			this.findIntersectors($(this.dummyMoment) , '.modal-moment-container');

		},

		findIntersectors : function (targetSelector, intersectorsSelector) {

		    var intersectors = [];

		    var $target = $(targetSelector);
		    var tAxis = $target.offset();
		    var t_x = [tAxis.left, tAxis.left + $target.outerWidth()];
		    var t_y = [tAxis.top, tAxis.top + $target.outerHeight()];


		    $(intersectorsSelector).each(_.bind(function(c,ele) {
		        var $this = $(ele);
		        var thisPos = $this.offset();
		        var i_x = [thisPos.left, thisPos.left + $this.outerWidth()]
		        var i_y = [thisPos.top, thisPos.top + $this.outerHeight()];

		       	if ( t_x[0] < i_x[1] && t_x[1] > i_x[0] && t_y[0] < i_y[1] && t_y[1] > i_y[0]) {

					this.imageDroppingArea($this)

		        }

		    },this));
		    // return intersectors;
		},
		imageDroppingArea: function(ele){

			if(!$(ele).hasClass('dummyMoment') && $(ele)!=this.currentMoment){

				if(this.currentMouseX < $(ele).offset().left + $(ele).width()/2 ){
					$(this.currentMoment).insertBefore($(ele));
				}else{
					$(this.currentMoment).insertAfter($(ele));
				}

			}

		},

		changeMoment : function(){
			if($('.new-moment-container').parents('.create-all-moments').length > 0){

				var prev_moment = $('.new-moment-container').prev();

				var moment_block = prev_moment.length > 0 ? prev_moment : $('.new-moment-container').next();

				var moment_cid = _.last(moment_block.attr('id').split('-'));

				var moment_model = this.tripModel.get('moments').get(moment_cid);

				var index = prev_moment.length > 0 ? moment_model.collection.indexOf(moment_model) + 1 : 1;

				var newMoment = new MomentModel(moment_model.pick('day_num'));
			
				_.each(this.selectedPhotos, function(photo){
					photo.set('moment', newMoment);
				});

				moment_model.collection.add(newMoment, {at : index});

			}
			this.remove();
			this.onCompleted();
		},
		remove: function(){
			$('.new-moment-container').off('mousedown');
			$('.modal').off('mouseup');			
		}

	});

	
	return CreateMoment;
});