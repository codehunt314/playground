/*jshint curly: false, multistr:true */
/*global define:false */
define(function (require) {
	'use strict';

	var $ = require('jquery'),
		_ = require('underscore'),
		Backbone = require('backbone-package'),
		PhotoModel = require('models/photo');

	require('text!templates/photo-preview.html');

	var PhotoPreview = Backbone.View.extend({
		className : 'photo-preivew',
		template: _.template(require('text!templates/photo-preview.html')),
		events : {
			'click .preview-image-thumb'	: 'imageClicked',
			'click .preview-delete-photo' 	: "deletePhoto",
			'click .preview-undo-delete-photo' 	: "undeletePhoto"
		},
		initialize: function(options){
			this.photo_id = options.photo_id;
			$(document).on('keydown.navigation', {self: this}, this.keyPressed);

			this.keyPressedDirection = 'none';
			this.selectedImageClass = ".preview-image-thumb.selected";
			this.preloadImagesArray = [];
		},
		serialize: function(){
			this.photoModel = PhotoModel.findOrCreate({id: this.photo_id});
			this.tripModel = this.photoModel.get('moment').get('trip');

			return {
				moments  : 	this.tripModel.get('moments').toJSON()
			};
		},
		render: function(){
			this.$el.html(this.template(this.serialize()));
			this.setImageWidth();
			$('#preview-modal-photo-'+this.photo_id).trigger('click');

			return this;
		},
		setImageWidth: function(){
			_.each($('.preview-image-thumb'),function(el){
					$(el).css('width',$(el).attr('ori-width')*80/$(el).attr('ori-height'));
			},this);
		},

		deletePhoto: function(){
			this.photoModel.set('deleted', true);
			$('#photo-'+this.photo_id).removeClass('selected').addClass('deleted');
			this.checkdeleteBtn();
		},

		undeletePhoto: function(){
			this.photoModel.set('deleted', false);
			$('#photo-'+this.photo_id).removeClass('deleted');
			this.checkdeleteBtn();
		},
		checkdeleteBtn: function(){
			if(this.photoModel.get('deleted')){
				$('.preview-undo-delete-photo').removeClass('hide');
				$('.preview-delete-photo').addClass('hide');
				$('.preview-photo-option').addClass('delete');
			}else{
				$('.preview-undo-delete-photo').addClass('hide');
				$('.preview-delete-photo').removeClass('hide');
				$('.preview-photo-option').removeClass('delete')
			}
		},
		keyPressed: function(e){

			var self = e.data.self;

			switch(e.which) {
				case 37:
					self.showPrev(e);
					this.keyPressedDirection = 'right';
					break;
				case 39:
					self.showNext(e);
					this.keyPressedDirection = 'left';
					break;
				default:
					break;
			}
		},
		showNext: function(e){
			$(this.selectedImageClass).next().trigger('click');
		},

		showPrev: function(e){
			$(this.selectedImageClass).prev().trigger('click');
		},

		imageClicked: function(e){
			this.photo_id = _.last($(e.currentTarget).attr('id').split('-'))
			this.photoModel = PhotoModel.findOrCreate({id: this.photo_id});

			this.$('.horizontal-loader').removeClass('hide');

			$('.preview-image-thumb').removeClass('selected');
			$(e.currentTarget).addClass('selected');

			this.checkdeleteBtn();

			var img = $('<img src="'+this.photoModel.get('url_l')+'"/>');
			img.load(this.imageLoaded.bind(this));

			this.preloadImages();
		},
		imageLoaded: function(img){

			this.$('.horizontal-loader').addClass('hide');
			$('#big-photo-image').attr('src',this.photoModel.get('url_l'));
			// 	setTimeout(function(){$('#big-photo-image').addClass('animate')}, 300);
		},

		preloadImages: function(){
			this.preloadImagesArray = [];
			if(this.keyPressedDirection=='left'){
				if($(this.selectedImageClass).next().length > 0 )this.preloadImagesArray.push($(this.selectedImageClass).next());
				if($(this.selectedImageClass).prev().length > 0 )this.preloadImagesArray.push($(this.selectedImageClass).prev());
				if($(this.selectedImageClass).prev().prev().length > 0 )this.preloadImagesArray.push($(this.selectedImageClass).prev().prev());
				if($(this.selectedImageClass).prev().prev().prev().length > 0 )this.preloadImagesArray.push($(this.selectedImageClass).prev().prev().prev());
			}else{
				if($(this.selectedImageClass).prev().length > 0 )this.preloadImagesArray.push($(this.selectedImageClass).prev());
				if($(this.selectedImageClass).next().length > 0 )this.preloadImagesArray.push($(this.selectedImageClass).next());
				if($(this.selectedImageClass).next().next().length > 0 )this.preloadImagesArray.push($(this.selectedImageClass).next().next());
				if($(this.selectedImageClass).next().next().next().length > 0 )this.preloadImagesArray.push($(this.selectedImageClass).next().next().next());
			}
			var id , photo;
			_.each(this.preloadImagesArray,function(el){
				id = _.last($(el).attr('id').split('-'));
				photo  = PhotoModel.findOrCreate({id: id});
				$('<img/>')[0].src = photo.get('url_l');
			});
		},
		remove: function(){
			$(document).off('keydown.navigation');
		}

	});


	return PhotoPreview;
});