/*global define:false */
define(function(require){
	'use strict';
	var Backbone = require('backbone-package'),
		_ = require('underscore'),
		Photo = require('models/photo');

	var Photos = Backbone.Collection.extend({
		id : '_id',
		model: Photo,
		comparator : 'order',
		jsonToServer: function(){
			this.sort();
			return _.map( this.where({deleted: false}), function(photo){
				return photo.id;
			});
		},
		jsonForPreview: function(){
			this.sort();
			return _.map( this.where({deleted: false}), function(photo){
				return photo.toJSON();
			});
		}
	});
	return Photos;
});