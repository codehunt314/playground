/*global define:false */
define(function(require){
	'use strict';
	var Backbone = require('backbone-package'),
		Trip = require('models/trip');

	var Trips = Backbone.Collection.extend({
		id : '_id',
		model: Trip
	});
	return Trips;
});