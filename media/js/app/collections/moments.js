/*global define:false */
define(function(require){
	'use strict';
	var Backbone = require('backbone-package'),
		_ = require('underscore'),
		Moment = require('models/moment');

	var Moments = Backbone.Collection.extend({
		id : '_id',
		model: Moment,
		jsonToServer: function(){
			return _.map(this.where({deleted:false}), function(moment){
				var coverPicDict = moment.has('cover-pic') ? {'cover-pic': moment.get('cover-pic').id} : {};
				return _.extend({},
					moment.pick('title', 'about', 'types', 'location'),
					{'photos': moment.get('photos').jsonToServer()},
					coverPicDict
				);
			});
		}
	});
	return Moments;
});