/*global define: false*/
define(['underscore', 'backbone', 'backbone-relational'], function (_, Backbone) {
	'use strict';
	return Backbone;
});