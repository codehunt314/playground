/* global jQuery: false, console:false*/
// the semi-colon before function invocation is a safety net against concatenated
// scripts and/or other plugins which may not be closed properly.
;
(function($, window, document, undefined) {
	'use strict';
	// undefined is used here as the undefined global variable in ECMAScript 3 is
	// mutable (ie. it can be changed by someone else). undefined isn't really being
	// passed in so we can ensure the value of it is truly undefined. In ES5, undefined
	// can no longer be modified.

	// window and document are passed through as local variable rather than global
	// as this (slightly) quickens the resolution process and can be more efficiently
	// minified (especially when both are regularly referenced in your plugin).

	$.fn.extend({
		getPosition: function(relative){
			if ( !this[0] ) {
				return null;
			}

			var elem = this.eq(0),
				offset = elem.offset(),
				scroll = {'left':elem.scrollLeft(), 'top':elem.scrollTop()},
				position = {
					'left': offset.left - scroll.left,
					'top' : offset.top - scroll.top
				};

			if (relative){
				var relativePosition = relative.getPosition();
				return {
					'left': position.left - relativePosition.left -
								(parseFloat( jQuery.css(relative[0], "borderLeftWidth") ) || 0),
					'top' : position.top - relativePosition.top -
								(parseFloat( jQuery.css(relative[0], "borderTopWidth") ) || 0)
				}
			}
			return position;
		},
		getCoordinates: function(element){
			//if (isBody(this)) return this.getWindow().getCoordinates();
			if ( !this[0] ) {
				return null;
			}

			var elem = this.eq(0);
			var position = elem.getPosition(element),
				size = {'width':elem.outerWidth(), 'height': elem.outerHeight()};
			var obj = {
				left: position.left,
				top: position.top,
				width: size.width,
				height: size.height
			};
			obj.right = obj.left + obj.width;
			obj.bottom = obj.top + obj.height;
			return obj;
		}
	});


	// Create the defaults once
	var pluginName = 'momentType';

	// The actual plugin constructor

	function Plugin(element, options) {
		var defaults = {
			showLocation: true,
			initText: 'What is this moment based around',
			minLengthForNewMomentType: 1,
			selectedMomentTypes : []
		};
		this.element = element;
		this.$el = $(element);
		this.options = $.extend({}, defaults, options);
		this.data = this.options.momentTypes;
		this._defaults = defaults;
		this._name = pluginName;
		this.init();
	}

	Plugin.prototype = {
		init: function() {
			this.initUI();
		},
		initUI: function(){
			this.$el.empty().addClass('moment-type-selection');
			if (this.options.showLocation) {
				this.showLocation();
			}

			this.$selectedMomentsDiv = $('<div />', {
				class : 'selected-moment-types'
			}).on('click', '.delete-moment-type', $.proxy(this.deleteMomentType, this)).appendTo(this.$el);

			this.$input = $('<input/>', {
				placeholder: this.options.initText,
				type: 'text',
				on : {
					focus : $.proxy(this.showSuggests, this),
					keyup : $.proxy(this.showSuggests, this),
					blur : $.proxy(this.inputLostFocus, this),
					keydown : $.proxy(this.keydown, this)
				}
			});
			this.$el.append(this.$input);

			this.$suggestContainer = $('<div />', {
				class : 'moment-suggest-container'
			}).appendTo(this.$el);
			this.$suggest = $('<ul />', {
				class : 'moment-suggest-ul'
			}).appendTo(this.$suggestContainer);
			this.$suggest.on('click', 'li', $.proxy(this.itemClicked, this));
			this.$suggest.on('mouseenter', 'li', $.proxy(this.mouseEnter, this));
			this.$suggest.on('mouseleave', 'li', $.proxy(this.mouseLeave, this));

			this.$childSuggest = $('<ul />', {
				class : 'moment-child-suggest-ul'
			}).appendTo(this.$suggestContainer);
			this.$childSuggest.on('click', 'li', $.proxy(this.itemClicked, this));
			this.$childSuggest.on('mouseenter', $.proxy(this.mouseEnterChildList, this));
			this.$childSuggest.on('mouseenter', 'li', $.proxy(this.mouseEnter, this));
			this.$childSuggest.on('mouseleave', 'li', $.proxy(this.mouseLeave, this));


			this.$addOption = $('<div />', {
				class : 'moment-suggest-add',
				html : 'Press &ldquo;Enter&rdquo; to add this a new moment type'
			}).appendTo(this.$el);


			if(this.options.selectedMomentTypes.length){
				var self = this;
				$.each(this.options.selectedMomentTypes, function(index, obj){
					self.renderMomentType(obj);
				});
			}
			/*this.$flatSuggest = $('<ul />', {
				class : 'moment-flat-suggest-ul'
			}).appendTo(this.$el);
			this.$suggest.on('click', 'li', $.proxy(this.itemClicked, this));
			this.$suggest.on('mouseenter', 'li', $.proxy(this.mouseEnter, this));
			this.$suggest.on('mouseleave', 'li', $.proxy(this.mouseLeave, this));*/
		},
		// Function to show location box at top. Basically this will help in autosuggest of the place.
		showLocation: function(initData) {
			// TODO
			console.log(initData);
		},
		// Shows autosuggest, triggered when user focuses on input,
		// functionality to show and hide suggest should be in this function
		// functionality to check if the value is changed or not.
		showSuggests: function(){
			var value = this.$input.val().trim();
			if(this.prevValue === value){
				return;
			}
			this.newMomentTypeMode = false;
			this.prevValue = value;
			var results = this.filterResults(value);
			if(value.length){
				if(results.length){
					this.showFilteredResults(results);
				}else{
					this.noMatchFound();
				}
				this.showAddOption();
			}else{
				this.hideAddOption();
				this.showFilteredResults(results);
			}
		},
		// filter the momenttypes based on the input
		filterResults: function(value){
			var matcher = new RegExp('\\b' + value.toLowerCase(), 'gi');
			if (value.length === 0) {
				return this.data.filter(function(item){
					return item.type === 'category';
				});
			}
			return this.data.filter($.proxy(function(item) {
				// TODO : make selection of title field configurable from options.
				var text = item.title;
				return matcher.test(text) ? true : false;
			}, this));
		},
		// Currently idea is to show all momenttype in two level navigation, just like amazon browse,
		// this function is triggered only when user has just focused on input and hasn't entered anything.
		// not sure if this will be final implementation, but nevertheless a good start.
		/*showAllTypes: function(){
			this.$suggest.show().empty().removeClass('child-shown').scrollTop(0);
			this.$childSuggest.show().removeClass('is-shown');
			this.$flatSuggest.hide();
			this.isAllShown = true;
			this.data.forEach($.proxy(function(item){
				if(item.type==='category'){
					this.$suggest.append(this.createParentType(item));
				}
			},this));
		},*/
		// if user start typing anything, we'll show results in a flat structure.
		showFilteredResults: function(results){
			// this.$suggest.hide();
			// this.$childSuggest.hide();
			this.hideChildTypes();
			this.$suggestContainer.show().children('.no-results-message').remove();
			this.$suggest.empty().scrollTop(0);
			// this.isAllShown = false;
			results.forEach($.proxy(function(item){
				var parent;
				if(item.type !== 'category'){
					parent = this.data.filter(function(type){
						return type.name === item.type;
					});
					if(parent.length === 1){
						parent = parent[0];
					}else{
						parent = null;
					}
					$('<li />', {
						class : 'li-item li-item-subcategory',
						html :  '<span class="title">'+item.title+'</span> in <span class="category">'+parent.title+'</span>'
					}).data('item',item).appendTo(this.$suggest);
				}else{
					this.$suggest.append(this.createParentType(item));
				}
			}, this));
		},
		/**
		 * function to make dom node of parent category
		 * @param  {object} item Is an object of moment-type json
		 * @return {jqueryDomNode}
		 */
		createParentTypeForNewMomentType: function(item){
			return $('<li />', {
				class: 'moment-parent-type-choosing li-item',
				html : '<i class="moment-icon serai-moment-'+item.name+'"></i> ' + item.title
			}).data('item', item);
		},
		/**
		 * function to make dom node of parent category
		 * @param  {object} item Is an object of moment-type json
		 * @return {jqueryDomNode}
		 */
		createParentType: function(item){
			return $('<li />', {
				class: 'moment-parent-type li-item',
				html : '<i class="moment-icon serai-moment-'+item.name+'"></i> ' + item.title
			}).data('item', item);
		},
		/**
		 * function to make dom node of parent category
		 * @param  {object} item Is an object of moment-type json
		 * @return {jqueryDomNode}
		 */
		createChildType: function(item){
			return $('<li />', {
				class: 'moment-child-type li-item',
				html : '<i class="moment-icon serai-moment-'+item.name+'"></i> ' + item.title
			}).data('item', item);
		},

		/*
		called when user clicks on any li, should trigger appropriate changes to selection and UI.
		 */
		itemClicked: function(e){
			var $el = $(e.target);
			if(this.shouldHide){
				clearTimeout(this.shouldHide);
			}
			if($el.hasClass('moment-parent-type')){
				this.showChildTypes($el.data('item'));
				this.currentParent = $el.data('item');
				$el.siblings().removeClass('active');
				$el.addClass('active');
				setTimeout($.proxy(function(){this.$input.focus();}, this), 50);
			}else if($el.hasClass('moment-parent-type-choosing')){
				this.selectOption(this.$input.val().trim(), $el);
				this.inputLostFocus();
			}else{
				this.selectOption($el);
				this.inputLostFocus();
			}
			console.log(e.target);
		},
		keydown: function(e){
			if (e.which === 38) { // Up Arrow Key
				e.preventDefault();
				this.moveSelection('up');
			} else if (e.which === 40) { // down Arrow
				e.preventDefault();
				this.moveSelection('down');
			} else if (e.which === 37) { // left Arrow
				this.shiftFocus('left', e);
			} else if (e.which === 39) { // right Arrow
				this.shiftFocus('right', e);
			} else if (e.which === 13) { // Enter Key
				this.enterPressed(e);
			} else if (e.which === 27) { // Esc Key
				this.$input.blur();
			}
		},
		inputLostFocus: function(){
			this.shouldHide = setTimeout($.proxy(function(){
				this.$input.val('');
				this.$suggestContainer.hide();
				this.$addOption.hide();
				delete this.prevValue;
				this.newMomentTypeMode = false;
			}, this), 200);
		},
		mouseEnter: function(e){
			var $el = $(e.currentTarget);
			// is a parent type, so show child types
			if($el.hasClass('moment-parent-type')){
				var delay = this.currentParent ? 200 : 0;
				if(this.showChildTimer){
					clearTimeout(this.showChildTimer);
				}
				this.showChildTimer = setTimeout($.proxy(function(){
					this.showChildTypes($el.data('item'));
					this.currentParent = $el.data('item');
					$el.siblings().removeClass('active');
					$el.addClass('active');
				}, this), delay);
			}else{
				if($el.parent().hasClass('moment-suggest-ul')){
					this.currentParent = null;
					$el.siblings().removeClass('active');
					this.hideChildTypes();
				}
				$el.addClass('active');
			}

		},
		mouseLeave: function(e){
			var $el = $(e.currentTarget);
			if($el.hasClass('moment-parent-type')){
				if(this.currentParent.name !== $el.data('item').name){
					$el.removeClass('active');
					// this.hideChildTypes($el.data('item'));
				}
			}else{
				$el.removeClass('active');
			}
		},
		mouseEnterChildList: function(e){
			var $el = $(e.currentTarget);
			if($el.data('parent').name === this.currentParent.name){
				clearTimeout(this.showChildTimer);
			}
		},
		// show child moment types for a specific parent
		showChildTypes: function(parent){
			this.isChildrenShown = true;
			this.$suggest.addClass('child-shown');
			this.$childSuggest.empty().show().addClass('is-shown').data('parent', parent);
			this.data.forEach($.proxy(function(item){
				if(item.type === parent.name){
					this.$childSuggest.append(this.createChildType(item));
				}
			},this));
		},
		hideChildTypes: function(){
			this.isChildrenShown = false;
			this.$suggest.removeClass('child-shown');
			this.$childSuggest.empty().removeClass('is-shown');
		},
		moveSelection: function(direction){
			var ul;
			if(this.isChildrenShown && this.$childSuggest.children('.active').length){
				ul = this.$childSuggest;
			} else {
				ul = this.$suggest;
			}

			var	current = ul.children('li.active'),
				next, delta, focusedItemCoordinates, scrollTop, top;

			if (direction === 'down') {
				next = current.length === 0 || current.next().length === 0 ? // already selected some
				ul.children('li.li-item').first() : current.next();
			} else {
				next = current.length === 0 || current.prev().length === 0 ? // already selected some
				ul.children('li.li-item').last() : current.prev();
			}

			// logic to place selected item
			focusedItemCoordinates = next.getCoordinates(ul);
		    scrollTop = ul.scrollTop();
		    delta = focusedItemCoordinates.bottom - ul.outerHeight();
		    scrollTop = ul.scrollTop();
		    top = focusedItemCoordinates.top;
		    if ((delta - scrollTop) > 0){
		        ul.scrollTop(delta);
		    }
		    if (scrollTop && scrollTop > top){
		        ul.scrollTop(top);
		    }

			current.removeClass('active');
			next.addClass('active');
			if(ul.hasClass('moment-suggest-ul')){
				if(next.hasClass('moment-parent-type')){
					this.showChildTypes(next.data('item'));
					this.currentParent = next.data('item');
				}else{
					this.hideChildTypes();
				}
			}
		},
		shiftFocus: function(direction, e){
			if(direction === 'right'){
				if(this.$childSuggest.children('.active').length === 0){
					this.$childSuggest.scrollTop(0).children('li.li-item').first().addClass('active');
					e.preventDefault();
				}
			}else{
				if(this.$childSuggest.children('.active').length !== 0){
					this.$childSuggest.children('li.li-item').removeClass('active');
					e.preventDefault();
				}
			}
		},

		noMatchFound: function(){
			this.$suggest.empty();
			this.$childSuggest.empty();
			$('<div />', {
				class : 'no-results-message',
				html : 'No match found, why don&lsquo;t you add this as a new moment type. Press enter to do this.'
			}).appendTo(this.$suggestContainer);
		},

		hideAddOption: function(){
			this.$addOption.removeClass('shown');
		},
		showAddOption: function(){
			this.$addOption.html('Press &ldquo;Enter&rdquo; to add this a new moment type').addClass('shown');
		},

		enterPressed: function(){
			if(this.$childSuggest.children('.active').length){
				this.selectOption(this.$childSuggest.children('.active'));
				return;
			}
			if(this.$suggest.children('.active.li-item-subcategory').length){
				this.selectOption(this.$suggest.children('.active.li-item-subcategory'));
			}
			if(this.newMomentTypeMode){
				this.selectOption(this.$input.val().trim(), this.$suggest.children('.active'));
			}else if( this.$input.val().trim().length >= this.options.minLengthForNewMomentType ){
				this.activateNewMomentAddition();
			}
		},

		selectOption: function(ele, type){
			var obj = {};
			if(typeof(ele) === 'string'){
				obj.name = type.data('item').name;
				obj.category = type.data('item').title;
				obj.subCategory = ele;
			}else{
				obj.name = ele.data('item').name;
				for(var i = 0 ; i < this.data.length ; i++ ){
					if ( this.data[i].name === ele.data('item').type ){
						obj.category = this.data[i].title;
						break;
					}
				}
				obj.subCategory = ele.data('item').title;
			}
			this.inputLostFocus();
			this.options.selectedMomentTypes.push(obj);
			this.renderMomentType(obj);
			this.intimateCallback();
			console.log('Selected : ', obj.name, obj.category, obj.subCategory);
		},

		renderMomentType: function(obj){
			$('<div />', {
				class : 'selected-moment-type',
				html : '<a href="#" class="delete-moment-type"><i class="fa fa-times-circle-o"></i></a><i class="moment-icon serai-moment-'+obj.name+'"></i><div class="moment-category">'+obj.category+'</div><div class="moment-subcategory">'+obj.subCategory+'</div>'
			}).data('obj', obj).appendTo(this.$selectedMomentsDiv);
			if( this.options.selectedMomentTypes.length >= this.options.maxSelection ){
				this.$input.blur().hide();
			}
		},

		deleteMomentType: function(e){
			e.preventDefault();
			var obj = $(e.target).parents('.selected-moment-type').data('obj'),
				selMoments = this.options.selectedMomentTypes,
				type;
			$(e.target).parents('.selected-moment-type').animate({
				'opacity': 0,
			}, 500, function(){
				$(this).remove();
			});
			for( var i = 0 ; i < selMoments.length ; i++ ){
				type = selMoments[i];
				if(type.name === obj.name && type.category === obj.category && type.subCategory === obj.subCategory){
					this.options.selectedMomentTypes.splice(i, 1);
					break;
				}
			}
			if( selMoments.length < this.options.maxSelection ){
				this.$input.show().focus();
			}
			this.intimateCallback();
		},

		activateNewMomentAddition: function(){
			this.$addOption.html('Select in which category should this belong').addClass('shown');

			this.newMomentTypeMode = true;

			this.hideChildTypes();
			this.$suggestContainer.show().children('.no-results-message').remove();
			this.$suggest.empty().scrollTop(0);

			var results = this.filterResults('');
			results.forEach($.proxy(function(item){
				if(item.type === 'category'){
					this.$suggest.append(this.createParentTypeForNewMomentType(item));
					this.$suggest.children('.li-item').first().addClass('active');
				}
			}, this));
		},

		intimateCallback: function(){
			if(this.options.callback){
				this.options.callback(this.options.selectedMomentTypes);
			}
		},

		focus: function(){
			this.$input.focus();
		}
	};


	// A really lightweight plugin wrapper around the constructor,
	// preventing against multiple instantiations
	$.fn[pluginName] = function(option) {
		var args = arguments,
			result;

		this.each(function() {
			var $this = $(this),
				data = $.data(this, 'plugin_' + pluginName),
				options = typeof option === 'object' && option;
			if (!data) {
				$this.data('plugin_' + pluginName, (data = new Plugin(this, options)));
			}
			// if first argument is a string, call silimarly named function
			// this gives flexibility to call functions of the plugin e.g.
			//   - $('.dial').plugin('destroy');
			//   - $('.dial').plugin('render', $('.new-child'));
			if (typeof option === 'string') {
				result = data[option].apply(data, Array.prototype.slice.call(args, 1));
			}
		});

		// To enable plugin returns values
		return result || this;
	};

})(jQuery, window, document);