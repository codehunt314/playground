/*global jQuery:false,_:false*/
(function($) {
    'use strict';

    var LocationSuggest = function(element, options) {
        this.init(element, options);
    };

    LocationSuggest.prototype = {
        defaults: {
            inputDelay : 200,
            minInput : 2,
            minInputMessage : 'Please enter 3 or more characters',
            noResultMessage : 'No result found',
            placeholder : 'Select a Place'
        },
        init: function(element, options) {
            this.options = $.extend({}, this.defaults, options || {});
            this.$element = $(element);
            this.$element.on({
                'keydown' : _.bind(this.keyDown, this),
                'focus' : _.bind(this.inputValueChanged, this),
                'keyup' : _.bind(this.inputValueChanged, this),
                'blur'  : _.bind(this.inputLostFocus, this)
            });
            if(this.options.placeholder){
                this.$element.attr('placeholder', this.options.placeholder);
            }

            this.$suggest = $('<ul />', {
                'class': 'location-suggests',
                'css': {
                    left: this.$element.position().left,
                    top : this.$element.position().top + this.$element.outerHeight(),
                    width : this.$element.outerWidth()
                }
            }).insertAfter(this.$element)
              .on('click', 'li', _.bind(this.itemClicked, this))
              .on('mouseenter', 'li', this.mouseEnter)
              .on('mouseleave', 'li', this.mouseLeave);

            this.$indicator = $('<div />',{
                html : '<div class="dot dot1"></div>\
                      <div class="dot dot2"></div>\
                      <div class="dot dot3"></div>\
                      <div class="dot dot4"></div>',
                'class' : 'loading-indicator loading-div loader loader-4',
                css : {
                    left: this.$element.position().left + this.$element.outerWidth() - 16 - 6,
                    top : this.$element.position().top + (this.$element.outerHeight() / 2 )
                }
            }).insertAfter(this.$element).hide();
        },
        keyDown: function(e){
            if (e.which === 38) { // Up Arrow Key
                e.preventDefault();
                this.moveSelection('up');
            } else if (e.which === 40) { // down Arrow
                e.preventDefault();
                this.moveSelection('down');
            } else if (e.which === 13) { // Enter Key
                this.selectItem(this.$suggest.children('li.li-item-active'));
            } else if (e.which === 27) { // Esc Key
                this.blur();
            }
        },
        moveSelection: function(direction) {
            var next,
                current = this.$suggest.children('li.li-item-active');
            if (direction === 'down') {
                next = current.length === 0 || current.next().length === 0 ? // already selected some
                this.$suggest.children('li:not(.li-message)').first() : current.next();
            } else {
                next = current.length === 0 || current.prev().length === 0 ? // already selected some
                this.$suggest.children('li:not(.li-message)').last() : current.prev();
            }
            current.removeClass('li-item-active');
            next.addClass('li-item-active');
        },
        inputValueChanged: function(e){
            if(this.prevValue !== this.$element.val() || this.$element.val().length < this.options.minInput){
                delete this.selectedPlace;
                if(this.prevTimer){clearTimeout(this.prevReq);}
                this.prevTimer = setTimeout(_.bind(this.fireAjax, this), this.options.inputDelay);
                this.prevValue = this.$element.val();
                this.fireCallback();
            }
        },
        inputLostFocus: function(e){
            setTimeout(_.bind(function(){
                if(!this.selectedPlace){ this.$element.val(''); }
                this.showHideSuggest(false);
            }, this), 100);
        },
        itemClicked: function(e){
            e.preventDefault();
            var ele = $(e.currentTarget);
            this.selectItem(ele);
        },
        selectItem: function(ele){
            if(ele.length){
                this.prevValue = ele.data('place').name;
                this.selectedPlace = ele.data('place');
                this.$element.val(ele.data('place').name);
                this.showHideSuggest(false);
                this.$element.blur();
                this.fireCallback();
            }
        },
        setValue: function(value) {
            if(typeof value === 'object'){
                this.selectedPlace = value;
                this.$element.val(value.name);
            }else{
                this.selectedPlace = value;
                this.$element.val(value);
            }
        },
        fireCallback: function(){
            if(this.options.callback){
                this.options.callback.call(this, this.selectedPlace || {});
            }
        },
        fireAjax: function() {
            var val = this.$element.val();
            if (val.length <= this.options.minInput) {
                this.showMinInputMessage();
            } else {
                this.showAjaxIndicator(true);
                if (this.prevReq) {
                    this.prevReq.abort();
                }
                var data = 'q=' + val + (this.options.type ? '&type=' + this.options.type : '');
                this.prevReq = $.ajax({
                    type: 'GET',
                    url: this.options.url,
                    dataType: 'json',
                    data: data,
                    success : _.bind(function(data){
                        this.showAjaxIndicator(false);
                        this.showPlaces(data);
                    }, this)
                });
            }
        },
        showMinInputMessage: function(){
            this.$suggest.html($('<li/>', {
                'html': this.options.minInputMessage,
                'class': 'li-message'
            }));
            this.showHideSuggest(true);
        },
        showAjaxIndicator: function(flag){
            this.$indicator[flag?'show':'hide']();
        },
        showPlaces: function(places){
            if (places.length > 0) {
                this.$suggest.empty();
                _.each(places, function(place) {
                    $('<li/>', {
                        'html': place.name,
                        'class': 'li-place-item'
                    }).appendTo(this.$suggest).data('place', place);
                }, this);
                this.showHideSuggest(true);
            } else {
                this.showPlaceNotFound();
            }
        },
        showHideSuggest: function(flag){
            if(flag){
                this.$suggest.css({
                    left: this.$element.position().left,
                    top : this.$element.position().top + this.$element.outerHeight(),
                });
            }
            this.$suggest[flag?'show':'hide']();
        },
        showPlaceNotFound: function(){
            this.$suggest.html($('<li/>', {
                'html': this.options.noResultMessage,
                'class': 'li-message'
            }));
            this.showHideSuggest(true);
        },
        mouseEnter: function() {
            $(this).siblings('.li-item-active').removeClass('li-item-active');
            $(this).addClass('li-item-active');
        },
        mouseLeave: function() {
            $(this).removeClass('li-item-active');
        }
    };

    $.fn.locationSuggest = function(option) {
        return this.each(function() {
            var $this = $(this),
                data = $this.data('location-suggest'),
                options = typeof option === 'object' && option;
            if (!data) $this.data('location-suggest', (data = new LocationSuggest(this, options)));
            if (typeof option == 'string') data[option]();
            else if (options.value) data.setValue(options.value);
        });
    };
})(jQuery);
