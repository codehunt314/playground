/*!
 * jquery.dragselect - v 1.0
 * Copyright (c) 2014 Nikesh Hayaran
 * Open Source MIT License
 */

/* global jQuery: true, console:false*/
(function($) {
	'use strict';
	var pluginName = 'dragnselect',
		defaults = {
			quality : 1,
			selectedClassName:'selected'
		};

	// The actual plugin constructor

	function Plugin(element, options) {
		this.element = element;
		this.$el = $(element);
		this.options = $.extend({}, defaults, options);
		this._defaults = defaults;
		this._name = pluginName;
		this._seletor = $("<div class='selector' >");
		this.top = 0;
		this.left = 0;
		this.crtlkeypressed = false;

		this.init();

	}

	Plugin.prototype = {
		init: function() {
			this.currentPhoto = null;
			this.changeOrder = false;
			this.isDragging = false;
			this.currentMouseX = 0;

			this.addEvents();

		},
		addEvents: function(){
			this.$el.on('mousedown.photoManagement', this.mouseDown.bind(this));
			this.$el.on('mouseup.photoManagement', this.mouseUp.bind(this));
			this.$el.on('mousedown.photoManagement', this.options.imageClass, this.imageMouseDown.bind(this));
			// $(this.options.imageClass).on('mousedown', this.imageMouseDown.bind(this));

			this.eventAdded = true;
		},
		removeEvents: function(){

			this.$el.off('mousedown.photoManagement');
			this.$el.off('mouseup.photoManagement');
			$(this.options.imageClass).off('mousedown');
			this.eventAdded = false;
		},
		refreshEvent: function(){
			// $(this.options.imageClass).each(function(c,ele) {
			// 	if(!$.hasData( ele )){
			// 		$(ele).on('mousedown', this.imageMouseDown.bind(this));
			// 	}
			// }.bind(this));
		},
		imageMouseDown: function(e){
			e.stopPropagation();

			this.currentPhoto = e.currentTarget;


			if(this.options.deletedClassName && $(this.currentPhoto).hasClass(this.options.deletedClassName))
				return;

			this.imageClicked = true;

			if(this.isDragging){
				$(this.currentPhoto).addClass(this.options.selectedClassName);
				if(this.options.imageSelected){
					this.options.imageSelected(this.currentPhoto);
				}
				return
			}
		},
		
		mouseDown: function(event){
			// event.preventDefault();
			// event.stopPropagation();
			if(event.button == 2)return;

			// if(this.changeOrder)return;

			this.canAddTimer = setTimeout($.proxy(function(){
				$(this.$el).append(this._seletor);
			}, this), 100);


			this.top = event.pageY ;
			this.left = event.pageX;

			this._seletor.css({"left":this.left ,"top": this.top});



			this.$el.on('mousemove.photoManagement', this.mouseMove.bind(this));
		},
		mouseUp:function(event){
			// event.preventDefault();
			// event.stopPropagation();

			// this.imageMoveFlag = false;
			this.$el.off('mousemove.photoManagement');
			this._seletor.css({'width':0 , 'height':0});
			this._seletor.remove();
			if(this.canAddTimer){
				clearTimeout(this.canAddTimer);
				delete this.canAddTimer;
			}




			if(this.imageClicked && !this.isDragging){

				if(	$(this.currentPhoto).hasClass(this.options.selectedClassName) ){
					$(this.currentPhoto).removeClass(this.options.selectedClassName);
				}else{
					$(this.currentPhoto).addClass(this.options.selectedClassName);
				}

				if(this.options.imageSelected)this.options.imageSelected(this.currentPhoto);
			}
			this.imageClicked = false;

			this.isDragging = false;
		},
		mouseMove: function(event){
			event.preventDefault();

			

			if(!this.isDragging){
				this.isDragging = true;
			}

			if(this.left > event.pageX){
				this._seletor.css({'left' : event.pageX , 'width':this.left - event.pageX});
			}else{
				this._seletor.css({'width':event.pageX - this.left});
			}

			if(this.top > event.pageY ){
				this._seletor.css({'top' : event.pageY , 'height':this.top - event.pageY });
			}else{
				this._seletor.css({'height':event.pageY  - this.top});
			}

			this.findIntersectors(this._seletor , this.options.imageClass, this, true);

		},

		findIntersectors : function (targetSelector, intersectorsSelector, self, dragSelection) {

		    var intersectors = [];

		    var $target = $(targetSelector);
		    var tAxis = $target.offset();
		    var t_x = [tAxis.left, tAxis.left + $target.outerWidth()];
		    var t_y = [tAxis.top, tAxis.top + $target.outerHeight()];


		    $(intersectorsSelector).each(function(c,ele) {
		        var $this = $(ele);
		        var thisPos = $this.offset();
		        var i_x = [thisPos.left, thisPos.left + $this.outerWidth()]
		        var i_y = [thisPos.top, thisPos.top + $this.outerHeight()];

		       	if ( t_x[0] < i_x[1] && t_x[1] > i_x[0] && t_y[0] < i_y[1] && t_y[1] > i_y[0]) {
		       		if(dragSelection){
						if(!$this.hasClass(self.options.selectedClassName)){
							$this.trigger( "mousedown" );
						}
						
					}
		        }
		    }.bind(this));
		    
		},

		restore: function(moment){
			var photos_obj 	= moment.get('photos').models,
				photosModel = [],
				newPhotosArray = [];


			_.each(photos_obj,function(ele,i){
				if(i==0){
					$('#moment-photos-'+moment.id).prepend( $('#photo-'+ele.id) );
				}else{
					$('#photo-'+ele.id).insertAfter($('#photo-'+photos_obj[i>0?i-1:0].id));
				}
			});
		}

	};

	// A really lightweight plugin wrapper around the constructor,
	// preventing against multiple instantiations
	$.fn[pluginName] = function(option) {
		var args = arguments,
				result;

		this.each(function() {
			var $this = $(this),
				data = $.data(this, 'plugin_' + pluginName),
				options = typeof option === 'object' && option;
			if (!data) {
				$this.data('plugin_' + pluginName, (data = new Plugin(this, options)));
			}
			// if first argument is a string, call silimarly named function
			// this gives flexibility to call functions of the plugin e.g.
			//   - $('.dial').plugin('destroy');
			//   - $('.dial').plugin('render', $('.new-child'));
			if (typeof option === 'string') {
				result = data[option].apply(data, Array.prototype.slice.call(args, 1));
			}
		});

		// To enable plugin returns values
    	return result || this;
	};

})(jQuery);