/* global define:false */
/*
	jQuery inPlaceEdit v1.0.0
	(c) 2013 Nitin Hayaran - 23spaces.com
	license: http://www.opensource.org/licenses/mit-license.php
*/
define(function (require) {
	'use strict';
	var $ = require('jquery'),
		_ = require('underscore');

	var NeatEdit = function(element, options) {
		this.init(element, options);
	};

	NeatEdit.prototype = {
		defaults: {
			className      : 'hover-can-edit',
			focusClassName : 'pulse',
			callback       : false,
			placeholder    : 'Edit me',
			onFocusClass   : 'can-edit-active',
			saveOnEnter    : true,
			saveOnBlur     : true,
			showLabel      : false,
			type           : 'textbox' // behave like textbox or textarea
		},

		init: function ( element, options ) {
			this.options = $.extend({}, this.defaults, options || {});
			this.$element = $(element);
			// onpaste='handlepaste(this, event)'
			this.$element[0].onpaste = $.proxy(this.handlePaste, this);
			this.$element
				.attr('spellcheck', 'false')
				// .addClass(this.options.className)
				.addClass('neat-edit-box');
			this.initialValue = this.$element.html();

			this.prepareDOM()
				.attachEvents()
				.handlePlaceHolder(this.getText().length === 0);
		},
		prepareDOM: function() {
			this.$element.empty();
			this.$p = $('<p/>', {
				class : 'neat-edit-content',
				html: this.initialValue || '<br>',
				contenteditable: 'true'
			}).appendTo(this.$element);
			return this;
		},
		attachEvents: function(){
			this.$p.on({
				'keydown.neatedit': $.proxy(this.keyDown, this),
                'keyup.neatedit': $.proxy(this.keyUp, this),
				'keypress.neatedit': $.proxy(this.keyPress, this),
				'focus.neatedit': $.proxy(this.onFocus, this),
				'blur.neatedit': $.proxy(this.onBlur, this)
				// 'activate.neatedit': $.proxy(this.onActivate, this)
			});
			return this;
		},
		handlePaste: function(e){
			e.preventDefault();
			e.stopPropagation();
			var text;
			var range = window.getSelection().getRangeAt(0),
				savedContent = this.$p.html();
			if(e.clipboardData) {
				text = e.clipboardData.getData('text/plain');
				this.processPaste(range, text);
			} else if(window.clipboardData){
				// IE event is attached to the window object
				text = window.clipboardData.getData('Text');
				this.processPaste(range, text);
			} else{
				this.$p.empty();
				this.waitForPaste(this.$p, savedContent, range);
			}
		},
		waitForPaste: function(elem, savedContent, range){
			if (elem.childNodes && elem.childNodes.length > 0) {
				this.$p.html(savedContent);
				this.processPaste(range, elem.text());
			}
			else {
				var that = {
					e: elem,
					r: range,
					s: savedContent
				};
				that.callself = function () {
					this.waitForPaste(that.e, that.s, that.r);
				};
				setTimeout(that.callself, 20);
			}
		},
		processPaste: function(range, text){
			var txtNode = document.createTextNode(text);
			range.insertNode(txtNode);
			range.collapse();
			var sel = window.getSelection();
			sel.removeAllRanges();
			sel.addRange(range);
		},
		isPrintable: function(e) {
			var keycode = e.which;
			var valid =
				(keycode > 47 && keycode < 58) || // number keys
				(keycode === 32) || (keycode === 13) || // spacebar & return key(s) (if you want to allow carriage returns)
				(keycode > 64 && keycode < 91) || // letter keys
				(keycode > 95 && keycode < 112) || // numpad keys
				(keycode > 185 && keycode < 193) || // ;=,-./` (in order)
				(keycode > 218 && keycode < 223); // [\]' (in order)
			return valid;
		},
		keyDown: function(e) {
			e.stopPropagation();
			this.options.onKeyDown && this.options.onKeyDown.call(this, e);
			if (e.which === 13) {
				this.handleEnter(e);
				return;
			}
			if (e.which === 27) {
				this.$p.html(this.initialValue);
				this.blur();
                return;
			}
			var valid = this.isPrintable(e);
			this.handlePlaceHolder(!(this.getText().length > 0 || valid));
		},
		keyUp: function(e) {
			this.options.onKeyUp && this.options.onKeyUp.call(this, e);
			e.stopPropagation();
			this.handlePlaceHolder(this.getText().length === 0);
            if(this.getHtml() !== this.prevValue){
                this.prevValue = this.getHtml();
                this.options.onChange && this.options.onChange.call(this, this.getHtml());
            }
		},
        keyPress: function(e){
			e.stopPropagation();
        },
        // this function can be called by the plugin caller to focus text field
        focus: function(){
			this.$element.children('p.neat-edit-content').focus();
        },
		onFocus: function() {
			this.options.onFocus && this.options.onFocus.call(this, this.getHtml());
            this.prevValue = this.getHtml();
			var range = document.createRange();
			range.selectNodeContents(this.$p.get(0));
			range.collapse();
			var selection = document.getSelection();
			selection.removeAllRanges();
			selection.addRange(range);
		},
        blur: function(){
			this.handlePlaceHolder(this.getText().length === 0);
            this.$p.trigger('blur');
        },
		onBlur: function() {
            setTimeout(_.bind(function(){
				this.options.onBlur && this.options.onBlur.call(this, this.getHtml());
			},this), 50);
			if(this.options.saveOnBlur){
				this.saveData();
			}
		},
		getText: function() {
			return this.$element.children('p.neat-edit-content').text();
		},
        getHtml: function(){
            if(this.options.type === 'textbox'){
                return this.$element.children('p.neat-edit-content').text();
            }
            else{
                var text = this.$element.children('p.neat-edit-content').text();
                if(text.trim().length>0){
                    return this.$element.children('p.neat-edit-content').html();
                }
                else{
                    return '';
                }
            }
        },
		handlePlaceHolder: function(flag) {
			var p;
			if (this.$element.children('p.placeholder').length === 0) {
				p = $('<p/>', {
					text: this.options.placeholder,
					class: 'placeholder',
					contenteditable: 'false',
					click : function(){
						$(this).prev('p.neat-edit-content').focus();
					}
				}).insertAfter(this.$p);
			}else{
				p = this.$element.children('p.placeholder');
			}
			if (flag) {
				if(this.options.showLabel){
					p.removeClass('label');
				}
				this.$p.css('min-width', p.outerWidth()+5);
			} else {
				if(this.options.showLabel){
					p.addClass('label');
				}else{
					p.remove();
	                this.$p.css('min-width', 'auto');
	            }
			}
		},
		handleEnter : function(e){
            e.preventDefault();
			if(this.options.type === 'textbox'){
				if(this.options.saveOnEnter){
					this.$p.trigger('blur');
					this.saveData();
				}else{

				}
			}else{
				if(this.options.saveOnEnter){
					if(!e.shiftKey){
						this.$p.trigger('blur');
						this.saveData();
					}
				}else{
                    // document.execCommand('insertHTML', false, '<br><br>');
                    document.execCommand('insertParagraph', false, '');
				}
			}
		},
		saveData: function(){
			var value = this.getHtml();
			if(this.initialValue !== value){
				this.initialValue = value;
				this.options.callback && this.options.callback.call(this, value, this.$element);
			}
		},
		destroy: function(){
			this.$element.html(this.getHtml())
				.off('.neatedit')
				.removeClass(this.options.className)
				.removeClass('neat-edit-box')
				.removeData('neatedit');
		},
		setValue: function(text){
			this.$p.html(text);
            var range = document.createRange();
            range.selectNodeContents(this.$p.get(0));
            range.collapse();
            var selection = window.getSelection();
            selection.removeAllRanges();
            selection.addRange(range);
			this.handlePlaceHolder(this.getText().length === 0);
		},
		clear: function(){
			this.initialValue = '';
			this.$p.empty();
			this.handlePlaceHolder(true);
		}
	};

	$.fn.neatEdit = function(option) {
		return this.each(function() {
			var $this = $(this),
				data = $this.data('neatedit'),
				options = typeof option === 'object' && option;
			if (!data) $this.data('neatedit', (data = new NeatEdit(this, options)));
			if (typeof option == 'string') data[option]();
			else if(options.value) data.setValue(options.value);
		});
	};
});