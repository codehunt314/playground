/*jshint white:false*/
/* global jQuery: true*/
/*"align_indent": true*/
// the semi-colon before function invocation is a safety net against concatenated
// scripts and/or other plugins which may not be closed properly.
;
(function($, window, document, undefined) {
    'use strict';
    // undefined is used here as the undefined global variable in ECMAScript 3 is
    // mutable (ie. it can be changed by someone else). undefined isn't really being
    // passed in so we can ensure the value of it is truly undefined. In ES5, undefined
    // can no longer be modified.

    // window and document are passed through as local variable rather than global
    // as this (slightly) quickens the resolution process and can be more efficiently
    // minified (especially when both are regularly referenced in your plugin).

    // Create the defaults once
    var pluginName = 'cluster';


    // The actual plugin constructor

    function Plugin(element, options) {
        this.element = element;
        this.$el = $(element);
        this._name = pluginName;
        this.init(options);
    }

    Plugin.prototype = {
        defaults: {
            trip_duration: 24*60*60,
            moment_duration: 60*60,
        },
        init: function(options) {
            this.options = $.extend({}, this.defaults, options);
            // this.options.photos.reverse();


            this.wDatePhotos = this.options.photos.reverse();
            this.woDatePhotos = [];
            this.checkDateTaken();
            this.trip_object = [];
            this.clusterTrips();
            this.clusterMoments();
        },
        checkDateTaken: function(){

            for(var j=0;j<this.wDatePhotos.length;j++){

                if(this.wDatePhotos[j].datetaken==null || this.wDatePhotos[j].datetaken==''){
                    this.woDatePhotos.push(this.wDatePhotos[j]);
                }
                
            }

            for(var i=0; i<this.woDatePhotos.length; i++){
                this.wDatePhotos.splice(this.woDatePhotos[i] - i,1);

            }
        },
        clusterTrips: function(){
           

            this.trips = this.photoCluster(this.wDatePhotos, this.options.trip_duration);
            this.flitered_trips = this.noiseFliter(this.trips);

            if(this.options.tripDataCallback){
                this.options.tripDataCallback(this.flitered_trips);
            }/*else{
                this.$el.html('');
                for(var i=0; i<this.flitered_trips.length; i++){
                    this.$el.append($('<div class="trip trip-'+i+'" ></div>').html(this.flitered_trips[i].length))
                }
            }*/
        },
        clusterMoments: function(){
            // console.log(this.photoCluster(this.flitered_trips[0], this.options.moment_duration));

            for(var i=1; i<=this.flitered_trips.length; i++){

                this.trip_object[i-1] = new Object;
                this.trip_object[i-1].id = i;
                this.trip_object[i-1].num_of_photos = this.flitered_trips[i-1].length;
                this.trip_object[i-1].serai_tripid = null;
                this.trip_object[i-1].title = null;
                this.trip_object[i-1].start_date = this.flitered_trips[i-1][0].datetaken;
                this.trip_object[i-1].end_date = this.flitered_trips[i-1][this.trip_object[i-1].num_of_photos-1].datetaken;
                this.trip_object[i-1].num_of_days = Math.ceil((new Date(this.trip_object[i-1].end_date) - new Date(this.trip_object[i-1].start_date))/(1000*60*60*24));
                
                // this.trip_object[i-1].moments = this.photoCluster(this.flitered_trips[i-1], this.options.moment_duration);
                var moments_photos = this.momentPhotoCluster(this.flitered_trips[i-1], this.options.moment_duration);
                var moments = []
                for(var j=0; j<moments_photos.length; j++){
                    var moment = new Object;
                    moment.title = null;
                    moment.num_of_photos = moments_photos[j].length;
                    moment.start_date = moments_photos[j][0].datetaken;
                    moment.end_date = moments_photos[j][moment.num_of_photos - 1].datetaken;
                    moment.duration = Math.ceil((new Date(this.trip_object[i-1].end_date) - new Date(this.trip_object[i-1].start_date))/(1000*60*60*24));
                    moment.photos = moments_photos[j];
                    moments.push(moment)
                }
                this.trip_object[i-1].moments = moments;
            }

            // var myString = JSON.stringify(trip_object);

            var jsondata = new Object();
            jsondata.trips = this.trip_object;
            jsondata.wodatephotos = this.woDatePhotos;

            if(this.options.callback){
                this.options.callback(jsondata);
            }
        },
        photoCluster : function(photos, duration){
            var count = 0 , cluster = [] ;
            cluster[count] = [];

            for(var j=0;j<photos.length;j++){

                if(j>0 && Math.abs(Date.parse(photos[j].datetaken) - Date.parse(photos[j-1].datetaken))/1000 > duration){
                    cluster[++count] = [];

                }
                
                cluster[count].push( photos[j] );
            }

            return cluster
        },

        noiseFliter: function(cluster){
            var cluster_length = cluster.length , k = 0 , cluster_to_fliter = [],number_of_days = 0,start_date,end_date,num_of_photos = 0;

            for(var i=0; i<cluster_length ; i++){
                num_of_photos   =   cluster[i].length;
                start_date      =   Date.parse(cluster[i][0].datetaken);
                end_date        =   Date.parse(cluster[i][cluster[i].length -1].datetaken);
                number_of_days  =   Math.ceil(Math.abs(end_date - start_date)/(1000*24*60*60));

               /* if(number_of_days <= 1){
                    cluster_to_fliter.push(i)
                }else if( num_of_photos/number_of_days < 10){
                    cluster_to_fliter.push(i)
                }*/

                if(num_of_photos<20 || (num_of_photos/number_of_days)<10){
                    cluster_to_fliter.push(i)
                }
                
            }

            for(var i=0; i<cluster_to_fliter.length; i++){
                cluster.splice(cluster_to_fliter[i] - i,1);

            }

            return cluster;
        },

        momentPhotoCluster : function(photos, duration){
            var count = 0 , cluster = [] , photo_time_difference = [] ;
            cluster[count] = []

            for(var j=0; j<photos.length; j++){
                if(j>0)photo_time_difference.push(Math.abs(Date.parse(photos[j].datetaken) - Date.parse(photos[j-1].datetaken))/1000);
            }

            var total = photo_time_difference.reduce(function(a, b) {return a + b;});
            duration  = Math.max(total/photo_time_difference.length , 10*60);

            for(var j=0; j<photos.length; j++){

                 if(j>0 && Math.abs(Date.parse(photos[j].datetaken) - Date.parse(photos[j-1].datetaken))/1000 > duration){
                    cluster[++count] = [];
                }
                cluster[count].push( photos[j] );

            }

            if(count>0){
                for(var j=0; j<=count; j++){
                    if(cluster[j].length <= 2){
                        
                        if(j==0){
                            cluster[j+1] = $.merge(cluster[j+1] , cluster[j]);
                        }else if(j==count){
                            cluster[j-1] = $.merge(cluster[j-1] , cluster[j]);
                        }else{
                            var diff = ( (Date.parse(cluster[j][0].datetaken)-Date.parse(cluster[j-1][cluster[j-1].length-1].datetaken)) < (Date.parse(cluster[j+1][0].datetaken)-Date.parse(cluster[j][cluster[j].length-1].datetaken)) )?true:false;
                            // diff ? cluster[j-1] : cluster[j+1] = diff ? $.merge(cluster[j-1], cluster[j]) : $.merge(cluster[j+1], cluster[j]);
                            if(diff){
                                cluster[j-1] = $.merge(cluster[j-1], cluster[j]);
                            }else{
                                cluster[j+1] = $.merge(cluster[j+1], cluster[j]);
                            }
                        }
                        
                        cluster.splice(j,1);
                        count--;j--;
                    }
                }
            }
            return cluster
        },

        refresh: function(options) {
            
        }
    };


    // A really lightweight plugin wrapper around the constructor,
    // preventing against multiple instantiations
    $.fn[pluginName] = function(option) {
        var args = arguments,
            result;

        this.each(function() {
            var $this = $(this),
                data = $.data(this, 'plugin_' + pluginName),
                options = typeof option === 'object' && option;
            // if (!data) {
                $this.data('plugin_' + pluginName, (data = new Plugin(this, options)));
            // }else{
            //     if (typeof option === 'string') {
            //         result = data[option].apply(data, Array.prototype.slice.call(args, 1));
            //     } else {
            //         data.refresh.call(data, options);
            //     }
            // }
        });

        // To enable plugin returns values
        return result || this;
    };

})(jQuery, window, document);
