$(document).ready(function(){



/*	$('.trip-title').on('click', tripClicked);
	$('.momentmeta').on('click', momentClicked);
*/
	

	// $('#trip-1').trigger('click');

	addEvents();
});	

var tripId = '-1';


/*function tripClicked(e){
	var tripId = $(e.currentTarget).attr('id').split('-')[1];
	if($('#trip-container-'+tripId).hasClass('hide')){
		$('.trip-container').addClass('hide');
		$('#trip-container-'+tripId).removeClass('hide');

		$('.trip-title','.trip').removeClass('selected');
		$(e.currentTarget).addClass('selected');
	}
	$(e.currentTarget).siblings('.moment-list').toggleClass('show');
}

function momentClicked(e){
	console.log(e);
}
*/

function addEvents(){
	$('.trip-item').on('click', showTripContent);
	$('.home-button').on('click', showTripsPage);
	$('.delete').on('mousedown' ,deleteMoment);
	$('.preview').on('mousedown' ,showMomentPreview);

	this.dragndrop = $('.trip-page').dragnselect({
						'imageClass':'.photo',
						'selectedClassName':'selected',
						'parentContainerClass':'trip-container'
						
					});

}

function showTripsPage(e){

	$('#trips-page').removeClass('hide');
	$('#trip-page').removeClass('show');
	$('.trip-container').addClass('hide');

}

function showTripContent(e){
	tripId = $(e.currentTarget).attr('id').split('-')[2];

	$('#trips-page').addClass('hide');
	$('#trip-page').addClass('show');
	$('#trip-'+tripId).removeClass('hide');



}

function showMomentPreview(e){
	e.stopPropagation();

	var moment = $(e.currentTarget).parents('.moment'),
		images =  [];
	$('.moment').removeClass('dropMoment');
	$('.movable',moment).removeClass('movable');

	$('.photo-img',moment).each(function(i,el){
		images.push($(el).css('background-image').replace('url(','').replace(')','' )) 
	})

	var trip_moment = $('.trip-moment-dummy').clone().removeClass('trip-moment-dummy hide');

	$('.restore',trip_moment).on('mousedown',restoreFunction);

	$('.moment-title',trip_moment).html($('.title',moment).text());

	$('.backgroundImage',trip_moment).css('background-image','url('+images[0]+')');



	$('.moment-media',trip_moment).html( $('.photo',moment).slice(0,9).clone()
											.addClass('photo_on_moments')
											.removeClass('photo')
											.on('mousedown', momentMediaClick) 
										);

	moment.append(trip_moment);

	

	$('.momentInnerContainer',moment).addClass('hide');


	
}

function deleteMoment(e){
	$(e.currentTarget).parents('.moment').addClass('deleted');
}

function momentMediaClick(e){
	e.stopPropagation();
}

function restoreFunction(e){
	$(e.currentTarget).parent().siblings().removeClass('hide');
	$(e.currentTarget).parents('.trip-moment').remove();
}





















