import os, json, httplib2, atom, time, urlparse
from datetime import datetime
import oauth, urllib
from django.http import HttpResponseRedirect, HttpResponse
from django.shortcuts import render_to_response
from django.views.decorators.csrf import csrf_exempt
from django.core.serializers.json import DjangoJSONEncoder
from sync.models import *
from sync import *
from django.http import StreamingHttpResponse
import base64, zlib
import zipfile
from StringIO import StringIO

try:
    from django.utils.six.moves import cPickle as pickle
except ImportError:
    import pickle

from stockphotos.settings import FLICKR_CALLBACK, DEMO_HOST, facebook_appid
from django.contrib.sessions.models import Session
from django.contrib.auth.models import User
from django.utils.encoding import force_bytes

def stream_response_generator():
    for x in range(1,24):
        yield '{} <br />\n'.format(x)
        time.sleep(1)

def home(request):
    user = get_user_from_session(request.session.session_key)
    print request.session.get('flickruserid')
    flickr_account          =   None
    if request.session.get('flickruserid'):
        flickr_userid           =   request.session.get('flickruserid')
        flickr_account          =   FlickrAccountSync.objects.get(userid=flickr_userid)
    elif user:
        try:
            flickr_account          =   FlickrAccountSync.objects.get(serai_userid=user.id)
        except:
            pass
    
    if flickr_account:
        trip_cluster_json       =   None
        if flickr_account.synced:
            trip_cluster_json       =   json.dumps([trip for trip in flickr_account.suggested_trips()], cls=DjangoJSONEncoder)
        if request.GET.get('fu', None):
            if request.GET.get('fu', None)==flickr_userid:
                trip_cluster_json       =   json.dumps([trip for trip in flickr_account.suggested_trips()], cls=DjangoJSONEncoder)
                return HttpResponse(trip_cluster_json)
            else:
                return HttpResponse('unauthorized request')
        return render_to_response('new-sync.html', {'account': flickr_account, 'profile': json.loads(flickr_account.profile), 'trip_cluster_json': trip_cluster_json, 'api_key' : flickr_api_key})
    else:
        featured_trip = json.loads(urllib.urlopen('%sapi/trip/featured/' % (DEMO_HOST)).read())
        return render_to_response('home.html', {'FLICKR_CALLBACK':FLICKR_CALLBACK, 'featured_trip': featured_trip})

def trip_home(request, tripid):
    return home(request)

def explore(request):
    if request.GET.get('u', None):
        flickr_userid       =   request.GET.get('u')
        fa, crtd            =   FlickrAccountSync.objects.get_or_create(userid=flickr_userid)
        if not fa.profile:
            fa.profile          =   json.dumps(fa.get_userinfo())
            fa.save()
        return render_to_response('new-sync.html', {'account': fa, 'profile': json.loads(fa.profile), 'trip_cluster_json': None, 'api_key' : flickr_api_key})
    if request.GET.get('fu', None):
        flickr_userid       =   request.GET.get('fu')
        fa, crtd            =   FlickrAccountSync.objects.get_or_create(userid=flickr_userid)
        trip_cluster_json       =   json.dumps([trip for trip in fa.suggested_trips()], cls=DjangoJSONEncoder)
        return HttpResponse(trip_cluster_json)


def flickr_get_request_token():
    params = {

        'oauth_timestamp'       :   str(int(time.time())),
        'oauth_signature_method':   "HMAC-SHA1",
        'oauth_version'         :   "1.0",
        'oauth_callback'        :   FLICKR_CALLBACK,
        'oauth_nonce'           :   oauth.generate_nonce(),
        'oauth_consumer_key'    :   flickr_api_key
    }
    consumer        =   oauth.OAuthConsumer(key=flickr_api_key, secret=flickr_api_secret)
    req             =   oauth.OAuthRequest(http_method="GET", http_url=req_token_url, parameters=params)
    signature       =   oauth.OAuthSignatureMethod_HMAC_SHA1().build_signature(req,consumer,None)
    req.set_parameter('oauth_signature', signature)
    content         =   urllib.urlopen(req.to_url()).read()
    request_token   =   dict(urlparse.parse_qsl(content))
    return request_token


def flickr_oauthcallback(request):
    if not (request.GET.get('oauth_token') and request.GET.get('oauth_verifier')):
        request_token                       =   flickr_get_request_token()
        request.session['request_token']    =   request_token
        url_to_authorize                    =   "%s?oauth_token=%s&perms=read" % (authorize_url, request_token['oauth_token'])
        return HttpResponseRedirect(url_to_authorize)
    else:
        oauth_token         =   request.GET.get('oauth_token')
        oauth_token_secret  =   request.session['request_token'].get('oauth_token_secret')
        token               =   oauth.OAuthToken(oauth_token, oauth_token_secret)
        access_token_parms  =   {
                'oauth_consumer_key'    :   flickr_api_key,
                'oauth_nonce'           :   oauth.generate_nonce(),
                'oauth_signature_method':   "HMAC-SHA1",
                'oauth_timestamp'       :   str(int(time.time())),
                'oauth_token'           :   oauth_token,
                'oauth_verifier'        :   request.GET.get('oauth_verifier')
            }
        consumer                =   oauth.OAuthConsumer(key=flickr_api_key, secret=flickr_api_secret)
        req                     =   oauth.OAuthRequest(http_method="GET", http_url=access_token_url, parameters=access_token_parms)
        signature               =   oauth.OAuthSignatureMethod_HMAC_SHA1().build_signature(req,consumer,token)
        req.set_parameter('oauth_signature', signature)
        content                 =   urllib.urlopen(req.to_url()).read()
        access_token_resp       =   dict(urlparse.parse_qsl(content))
        flickr_account, created =   FlickrAccountSync.objects.get_or_create(
                                        userid      =   access_token_resp['user_nsid'],
                                        defaults    =   {'credential': json.dumps(access_token_resp)}
                                    )
        flickr_account.profile  =   json.dumps(flickr_account.get_userinfo())
        flickr_account.save()
        request.session['flickruserid']     =   access_token_resp['user_nsid']
        return HttpResponseRedirect('/playground/')


def get_user_from_session(session_key):
    try:
        session = Session.objects.get(session_key=session_key)
        encoded_data = base64.b64decode(force_bytes(session.session_data))
        hash, pickled = encoded_data.split(b':', 1)
        uid = pickle.loads(pickled).get('_auth_user_id')
        return User.objects.get(pk=uid)
    except:
        return None


def _get_date_range(photos):
    for p in photos:
        if not isinstance(p['datetaken'], datetime):
            p['datetaken'] = datetime.strptime(p['datetaken'], '%Y-%m-%d %H:%M:%S')
    start_date  =   min([j['datetaken'] for j in photos])
    end_date    =   max([j['datetaken'] for j in photos])
    return start_date, end_date


def _temp(trip, trip_post_data=None):
    all_trip_photos =   {}
    raw_trip        =   json.loads(trip.trip_data)
    for m in raw_trip.get('moments', []):
        for ph in m['photos']:
            #all_trip_photos[ph['id']]   =   dict([(k, ph[k]) for k in ['datetaken', 'height_o', 'width_o', 'url_o', 'id', 'title', 'url_m']])
            all_trip_photos[ph['id']]   =   dict([(k, ph[k]) for k in ph])
    trip_start_date, trip_end_date      =   _get_date_range(all_trip_photos.values())
    if trip_post_data:
        trip_data_dict                  =   json.loads(trip_post_data)
    else:
        trip_data_dict                  =   json.loads(trip.saved_trip)
    trip_data_dict['start_date']        =   trip_start_date
    trip_data_dict['end_date']          =   trip_end_date
    trip_data_dict['num_of_days']       =   (trip_end_date - trip_start_date).days + 1
    if trip_data_dict.get('cover-pic'):
        trip_data_dict['cover-pic']         =   all_trip_photos[trip_data_dict['cover-pic']]['url_o']
    for m in trip_data_dict['moments']:
        m['photos'] = [all_trip_photos[p] for p in m['photos']]
        moment_start_date, moment_end_date  =   _get_date_range(m['photos'])
        m['day'] = (moment_start_date - trip_start_date).days + 1
        if m.get('cover-pic'):
            cover_pic       =   all_trip_photos[m['cover-pic']]
            m['cover-pic']  =   {'default' : cover_pic['url_o'], 'medium': cover_pic['url_m'], 'small': cover_pic['url_m']}
    return trip_data_dict


def _temp1(trip, trip_post_data=None):
    all_trip_photos =   {}
    raw_trip = trip.get_trip_json()
    for m in raw_trip.get('moments', []):
        for ph in m['photos']:
            all_trip_photos[ph['photoid']]   =   ph
    if trip_post_data:
        trip_data_dict                  =   json.loads(trip_post_data)
    else:
        trip_data_dict                  =   json.loads(trip.saved_trip)
    trip_start_date, trip_end_date      =   _get_date_range(all_trip_photos.values())
    trip_data_dict['start_date']        =   trip_start_date
    trip_data_dict['end_date']          =   trip_end_date
    trip_data_dict['num_of_days']       =   trip.num_of_days
    if trip_data_dict.get('cover-pic'):
        cover_pic = model_to_dict(FlickrAccountPhoto.objects.get(photoid=trip_data_dict['cover-pic']))
        sizes = cover_pic.pop('sizes')
        cover_pic.update(json.loads(sizes))
        trip_data_dict['cover-pic']         =   cover_pic['url_o']
    for m in trip_data_dict['moments']:
        m['photos'] = [all_trip_photos[p] for p in m['photos']]
        moment_start_date, moment_end_date  =   _get_date_range(m['photos'])
        m['day'] = (moment_start_date - trip_start_date).days + 1
        if m.get('cover-pic'):
            cover_pic = model_to_dict(FlickrAccountPhoto.objects.get(photoid=m['cover-pic']))
            sizes = cover_pic.pop('sizes')
            cover_pic.update(json.loads(sizes))
            m['cover-pic']  =   {'default' : cover_pic['url_o'], 'medium': cover_pic['url_m'], 'small': cover_pic['url_m']}
    return trip_data_dict

@csrf_exempt
def savetriptoserai(request):
    user = get_user_from_session(request.session.session_key)
    if user:
        if request.method=='POST':
            trip_data           =   request.POST.get('data')
            trip_id             =   json.loads(trip_data)['id']
            trip_object         =   Trip.objects.get(id=int(trip_id))
            trip_data_dict      =   _temp1(trip_object, trip_data)
            trip_object.flickr_account.serai_userid = user.id
            trip_object.flickr_account.save()
            params  =   urllib.urlencode({'trip_data': json.dumps(trip_data_dict, cls=DjangoJSONEncoder), 'userid': user.id})
            f       =   urllib.urlopen('%sapi/trip/save_flickr_trip/'%(DEMO_HOST), params)
            result  =   f.read()
            trip_object.serai_tripid = result
            trip_object.save()

            print "save trip directly to serai"
            print "return to trip page"
            return  HttpResponseRedirect('%strip/%s/' % (DEMO_HOST, json.loads(result)['id']))

        else:
            print "reached to this section after oauth dance"
            if request.GET.get('flickrtrip'):
                flickrtripid    =   request.GET.get('flickrtrip')
                trip_object     =   Trip.objects.get(id=int(flickrtripid))
                trip_data_dict  =   _temp1(trip_object)
                trip_object.flickr_account.serai_userid = user.id
                trip_object.flickr_account.save()
                params  =   urllib.urlencode({'trip_data': json.dumps(trip_data_dict, cls=DjangoJSONEncoder), 'userid': user.id})
                f       =   urllib.urlopen('%sapi/trip/save_flickr_trip/'%(DEMO_HOST), params)
                result  =   f.read()
                trip_object.serai_tripid = result
                trip_object.save()
                print "now match trip and save it to serai"
                print "return to trip page"

                return  HttpResponseRedirect('%strip/%s/' % (DEMO_HOST, json.loads(result)['id']))

            print "Something happend on serai during oauth dance"
            return HttpResponse("Something happend on serai during oauth dance")
    else:
        if request.method=='POST':
            print "Saving trip locally"
            trip_data               =   request.POST.get('data')
            trip_data_dict          =   json.loads(trip_data)
            trip_id                 =   trip_data_dict['id']
            trip_object             =   Trip.objects.get(id=int(trip_id))
            trip_object.saved_trip  =   trip_data
            trip_object.save()
            print "redirecting to serai for registration"
            redirect_url = 'https://graph.facebook.com/oauth/authorize?client_id=%s&redirect_uri=%sfbchanges/auth/callback/facebook?flickrtrip=%s&scope=user_about_me,user_birthday,user_hometown,user_relationships,user_website,email,read_friendlists' % (facebook_appid, DEMO_HOST, trip_id)
            return HttpResponseRedirect(redirect_url)
        else:
            return HttpResponseRedirect("should not come here in any case, it means some issue with login mechanism")
            print "should not come here in any case, it means some issue with login mechanism"


@csrf_exempt
def savetrip(request):
    if request.method!='POST':
        return HttpResponse('wrong request')
    # user = get_user_from_session(request.session.session_key)
    # if request.session.get('flickruserid'):
    #     flickr_userid           =   request.session.get('flickruserid')
    #     flickr_account          =   FlickrAccountSync.objects.get(userid=flickr_userid)
    # elif user:
    #     flickr_account          =   FlickrAccountSync.objects.get(serai_userid=user.id)
    # else:
    #     return HttpResponse('not flickr account found, this messege should never occurs')

    zfile       =   zipfile.ZipFile(request.FILES['file'])
    trip_data   =   json.loads(zfile.read(zfile.namelist()[0]))

    if trip_data.get('user_id'):
        flickr_account, crtd = FlickrAccountSync.objects.get_or_create(userid=trip_data.get('user_id'))
    else:
        return HttpResponse('not flickr account found, this messege should never occurs')

    for ph in trip_data.get('woDatePhotos', []):
        ph_sizes = {}
        ph_copy = ph.copy()
        photoid = ph_copy.pop('id')
        ph_copy.pop('description')
        for k in ph:
            if k.find('width')!=-1 or k.find('height')!=-1 or k.find('url')!=-1:
                ph_sizes[k] = ph_copy.pop(k)
        ph_copy['sizes'] = json.dumps(ph_sizes)
        faph, crtd = FlickrAccountPhoto.objects.get_or_create(flickrac=flickr_account, photoid=photoid, defaults=ph_copy)

    for trip in trip_data.get('trips', []):
        trip_obj, crtd  =   Trip.objects.get_or_create(
                            flickr_account  =   flickr_account,
                            start_date      =   datetime.strptime(trip['start_date'], '%Y-%m-%d %H:%M:%S'),
                            end_date        =   datetime.strptime(trip['end_date'], '%Y-%m-%d %H:%M:%S'),
                            num_of_days     =   trip['num_of_days'],
                            num_of_photos   =   trip['num_of_photos'],
                            title           =   trip['title']
                        )
        moment_list = []
        for moment in trip['moments']:
            moment_photos = moment.pop('photos')
            moment_photos_idlist = []
            for ph in moment_photos:
                ph_sizes = {}
                ph_copy = ph.copy()
                photoid = ph_copy.pop('id')
                ph_copy.pop('description')
                for k in ph:
                    if k.find('width')!=-1 or k.find('height')!=-1 or k.find('url')!=-1:
                        ph_sizes[k] = ph_copy.pop(k)
                ph_copy['sizes'] = json.dumps(ph_sizes)
                faph, crtd  =   FlickrAccountPhoto.objects.get_or_create(flickrac=flickr_account, photoid=photoid, defaults=ph_copy)
                faph.trip   =   trip_obj
                faph.save()
                moment_photos_idlist.append(faph.id)
            moment['photos'] = moment_photos_idlist
            moment_list.append(moment)
        trip_obj.moment_cluster =   json.dumps(moment_list, cls=DjangoJSONEncoder)
        trip_obj.save()
    flickr_account.synced = True
    flickr_account.save()
    return HttpResponse('trip saved')


def autocomplete(request):
    query           =   request.GET.get('q', None)
    if query:
        params      =   urllib.urlencode({'input': query, 'key': 'AIzaSyAF2bTQ5RNq39mrXeX1gth9xAQyyanEFyw', 'sensor': 'false'})
        req_url     =   "https://maps.googleapis.com/maps/api/place/autocomplete/json?%s" % params
        results     =   json.loads(urllib.urlopen(req_url).read())
        suggestions =   []
        for p in results['predictions']:
            p_data = {'id': p.get('reference'), 'name': None}
            print p['description']
            if len(p['terms'])==1:
                p_data['name'] = p['terms'][0]['value']
            elif len(p['terms'])==2:
                p_data['name'] = '%s, %s'%(p['terms'][0]['value'], p['terms'][1]['value'])
            elif len(p['terms'])==3:
                p_data['name'] = '%s, %s, %s'%(p['terms'][0]['value'], p['terms'][1]['value'], p['terms'][2]['value'])
            else:
                p_data['name'] = '%s, %s, %s'%(p['terms'][0]['value'], p['terms'][-2]['value'], p['terms'][-1]['value'])
            suggestions.append(p_data)
        return HttpResponse(json.dumps(suggestions))
    return HttpResponse('wrong query')


@csrf_exempt
def feedback_mesg(request):
    if request.method=='POST':
        if request.POST.get('feedback'):
            Feedback.objects.get_or_create(mesg=request.POST.get('feedback'))
            return HttpResponse('Thanks for feedback')
    return HttpResponse('invalid request')


@csrf_exempt
def feedback_services(request):
    if request.method=='POST':
        services    =   request.POST.get('services')
        email       =   request.POST.get('email')
        ServicesUsed.objects.get_or_create(email=email, services=services)
        return HttpResponse('Thanks for feedback')
    return HttpResponse('invalid request')



