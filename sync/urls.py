from django.conf.urls import patterns, include, url

urlpatterns = patterns('',
    # Examples:
    url(r'^$', 'sync.views.home', name='sync'),
    url(r'^trip/(?P<tripid>[A-Z0-9]+)/$', 'sync.views.trip_home', name='trip_home'),
    url(r'^explore/$', 'sync.views.explore', name='explore'),
    url(r'^flickr/oauthcallback/$', 'sync.views.flickr_oauthcallback'),
    url(r'^feedback/message/$', 'sync.views.feedback_mesg'),
    url(r'^feedback/services/$', 'sync.views.feedback_services'),
    url(r'^locsearch/$', 'sync.views.autocomplete'),
    url(r'^savetriptoserai/$', 'sync.views.savetriptoserai'),
    url(r'^savetrip/$', 'sync.views.savetrip'),
)