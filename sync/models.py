from django.db import models

import pickle, httplib2, urllib, base64, json, atom, oauth, math
from datetime import datetime, timedelta
from django.contrib.auth.models import User
from oauth2client.django_orm import FlowField
from oauth2client.django_orm import CredentialsField
from oauth2client.client import OAuth2Credentials
from sync import *
from bs4 import BeautifulSoup
from collections import defaultdict
import calendar, time, oauth
from django.core.serializers.json import DjangoJSONEncoder
from django.forms.models import model_to_dict
from django.db.models import Q
from django.utils.timezone import utc



to_rad = math.pi / 180.0
def _distance(lat1,lng1,lat2,lng2):
    global to_rad
    earth_radius_km = 6371
    dLat = (lat2-lat1) * to_rad
    dLon = (lng2-lng1) * to_rad
    lat1_rad = lat1 * to_rad
    lat2_rad = lat2 * to_rad

    a = math.sin(dLat/2) * math.sin(dLat/2) + math.sin(dLon/2) * math.sin(dLon/2) * math.cos(lat1_rad) * math.cos(lat2_rad)
    c = 2 * math.atan2(math.sqrt(a), math.sqrt(1-a)); 
    dist = earth_radius_km * c
    return dist

def remove_corrupt_geodata(photos_list):
    geo_photos = filter(lambda p: float(p['latitude'])!=0.0 or float(p['longitude'])!=0.0, photos_list)
    distance_matrix = [(p1['id'], _distance(float(p1['latitude']), float(p1['longitude']), float(p2['latitude']), float(p2['longitude']))) for p1 in geo_photos for p2 in geo_photos]

def _get_geo_distribution(photos_list):
    distance  = 0
    geo_photos = filter(lambda p: float(p['latitude'])!=0.0 or float(p['longitude'])!=0.0, photos_list)
    if len(geo_photos)<=1:
        return distance
    else:
        start_lat, start_lng    =   float(geo_photos[0]['latitude']), float(geo_photos[0]['longitude'])
        start_time              =   geo_photos[0]['datetaken']#datetime.strptime(geo_photos[0]['datetaken'], '%Y-%m-%d %H:%M:%S')
        for p in geo_photos[1:]:
            cur_lat, cur_lng    =   float(p['latitude']), float(p['longitude'])
            cur_time = p['datetaken'] #datetime.strptime(p['datetaken'], '%Y-%m-%d %H:%M:%S')
            time_elapsed        =   (cur_time-start_time).total_seconds() + 1 # to avoid zero division error
            distance_coverd     =   _distance(start_lat, start_lng, cur_lat, cur_lng)
            if distance_coverd/time_elapsed<700:
                distance            +=  distance_coverd
                start_lat, start_lng =  cur_lat, cur_lng
        return distance


def _get_cluster(photos_list, diff_in_seconds):
    clusters        =   defaultdict(list)
    for ph in photos_list:
        try:
            ph['datetaken']     =   datetime.strptime(ph['datetaken'], '%Y-%m-%d %H:%M:%S')
        except:
            ph['datetaken']     =   None
    photos_list     =   filter(lambda x: x['datetaken'] is not None, photos_list)
    lists           =   [tuple(photos_list[i:i+2]) for i in xrange(0, len(photos_list))][:-1]
    diff_list       =   [(y['datetaken'] - x['datetaken']).total_seconds() for x,y in lists]
    j               =   0
    for i in xrange(0, len(photos_list)):
        if i>0 and diff_list[i-1]>diff_in_seconds:
            j += 1
        clusters[j].append(photos_list[i])
    return clusters


def _get_moment_cluster(photos_list, diff_in_seconds):
    clusters        =   defaultdict(list)
    for ph in photos_list:
        try:
            ph['datetaken']     =   datetime.strptime(ph['datetaken'], '%Y-%m-%d %H:%M:%S')
        except:
            ph['datetaken']     =   None
    photos_list     =   filter(lambda x: x['datetaken'] is not None, photos_list)
    lists           =   [tuple(photos_list[i:i+2]) for i in xrange(0, len(photos_list))][:-1]
    diff_list       =   [(y['datetaken'] - x['datetaken']).total_seconds() for x,y in lists]
    diff_in_seconds =   max(sum(diff_list)/len(diff_list), 14400)
    # import pdb; pdb.set_trace()
    j               =   0
    for i in xrange(0, len(photos_list)):
        if i>0 and ( diff_list[i-1]/float(diff_in_seconds) > 1.0):
            prev_cluster_photos = []
            if len(clusters[j])<=1:
                prev_cluster_photos = clusters.pop(j)
            j += 1
            for p in prev_cluster_photos:
                clusters[j].append(p)
        clusters[j].append(photos_list[i])
        if i==len(photos_list)-1:
            if len(clusters[j])<=2:
                prev_cluster_photos = clusters.pop(j)
                for p in prev_cluster_photos:
                    clusters[j-1].append(p)
    return clusters


class FlickrAccountSync(models.Model):
    userid          =   models.CharField(max_length=255)
    credential      =   models.TextField(blank=True, null=True)
    photos          =   models.TextField(blank=True, null=True)
    synced_time     =   models.DateTimeField(auto_now=True, auto_now_add=True)
    synced          =   models.BooleanField(default=False)
    profile         =   models.TextField(blank=True, null=True)
    last_page       =   models.PositiveIntegerField(default=0)
    serai_userid    =   models.PositiveIntegerField(default=0)

    def filckr_fetch_photos(self, page=1):
        flickr_rest_url             =   'https://api.flickr.com/services/rest'
        params                      =   {
                                            'oauth_consumer_key':   flickr_api_key,
                                            'user_id'           :   self.userid,
                                            'page'              :   page,
                                            'per_page'          :   500,
                                            'extras'            :   'date_taken,description,geo,tags,machine_tags,views,url_n,url_m,url_s,url_l,url_o,url_sq,url_z,o_dims'
                                        }
        if self.credential:
            credential              =   json.loads(self.credential)
            token                   =   oauth.OAuthToken(credential['oauth_token'], credential['oauth_token_secret'])
            params['oauth_token']   =   credential['oauth_token']
            params['method']        =   'flickr.people.getPhotos'
            consumer                =   oauth.OAuthConsumer(key=flickr_api_key, secret=flickr_api_secret)
            req                     =   oauth.OAuthRequest(http_method="GET", http_url=flickr_rest_url, parameters=params)
            signature               =   oauth.OAuthSignatureMethod_HMAC_SHA1().build_signature(req,consumer,token)
            req.set_parameter('api_sig', signature)
        else:
            params['method']        =   'flickr.people.getPublicPhotos'
            req                     =   oauth.OAuthRequest(http_method="GET", http_url=flickr_rest_url, parameters=params)

        content         =   urllib.urlopen(req.to_url()).read()
        soup            =   BeautifulSoup(content)

        for ph in soup.findAll('photo'):
            ph_sizes = {}
            ph_dict = ph.attrs.copy()
            photoid = ph_dict.pop('id')
            for k in ph.attrs:
                if k.find('width')!=-1 or k.find('height')!=-1 or k.find('url')!=-1:
                    ph_sizes[k] = ph_dict.pop(k)
            ph_dict['sizes'] = json.dumps(ph_sizes)
            fa, crtd = FlickrAccountPhoto.objects.get_or_create(flickrac=self, photoid=photoid, defaults=ph_dict)

        self.last_page = page
        self.save()


    def sync_flickr(self):
        profile         =   json.loads(self.profile)
        total_photos    =   profile['totalphotos']
        total_call      =   int(math.ceil(float(total_photos)/500))
        for i in range(self.last_page+1, total_call+1):
            self.filckr_fetch_photos(page=i)
        # self.photos     =   json.dumps([p for p in self.filckr_fetch_photos()], cls=DjangoJSONEncoder)
        self.synced     =   True
        self.save()

    def get_userinfo(self):
        try:
            credential          =   json.loads(self.credential)
            params              =   {
                                    'oauth_consumer_key':   flickr_api_key,
                                    'oauth_token'       :   credential['oauth_token'],
                                    'method'            :   'flickr.people.getInfo',
                                    'user_id'           :   self.userid,
                                    'oauth_timestamp'       :   str(int(time.time())),
                                    'oauth_signature_method':   "HMAC-SHA1",
                                    'oauth_nonce'           :   oauth.generate_nonce()
                                }
        except:
            params              =   {
                                    'oauth_consumer_key':   flickr_api_key,
                                    'method'            :   'flickr.people.getInfo',
                                    'user_id'           :   self.userid
                                }
        flickr_rest_url     =   'https://api.flickr.com/services/rest'
        req                 =   oauth.OAuthRequest(http_method="GET", http_url=flickr_rest_url, parameters=params)
        # h                   =   httplib2.Http()
        # resp, content       =   h.request(req.to_url(), "GET")
        content             =   urllib.urlopen(req.to_url()).read()
        soup                =   BeautifulSoup(content)
        icon_info           =   soup.find('person').attrs

        epoch           =   datetime(1970, 1, 1)
        firstdatetaken  =   datetime.strptime(soup.find('firstdatetaken').text, "%Y-%m-%d %H:%M:%S")
        if firstdatetaken<epoch:
            diff                    =   firstdatetaken - epoch
            firstdatetaken_value    =   diff.days * 24 * 3600 + diff.seconds
        else:
            firstdatetaken_value    =   int(time.mktime(firstdatetaken.timetuple()))
        username = soup.find('username').text
        try:
            realname = soup.find('realname').text
        except:
            realname = username
        if int(icon_info['iconserver'])>0:
            usericon = 'http://farm%s.staticflickr.com/%s/buddyicons/%s.jpg' % (icon_info['iconfarm'], icon_info['iconserver'], self.userid)
        else:
            usericon = 'https://www.flickr.com/images/buddyicon.gif'
        return {
                    'username': username,
                    'realname': realname,
                    'totalphotos': soup.find('count').text,
                    'firstdatetaken': firstdatetaken_value,
                    'usericon': usericon
        }


    def construct_trip(self, diff_in_seconds=86400):
        photos_qs   =   FlickrAccountPhoto.objects.filter(flickrac=self).filter(~Q(datetaken=None)).order_by('datetaken')
        photos_list =   photos_qs.values('id', 'title', 'tags', 'views', 'latitude', 'longitude', 'place_id', 'woeid', 'photoid', 'datetaken')
        clusters    =   _get_cluster(photos_list, diff_in_seconds)
        result = []
        print 'num of clusters : %d' %(len(clusters))
        for c in clusters:
            num_of_photos   =   len(clusters[c])
            start_date      =   min([j['datetaken'] for j in clusters[c]])
            end_date        =   max([j['datetaken'] for j in clusters[c]])
            num_of_days     =   math.ceil((end_date - start_date).total_seconds()/(24*60*60))
            if num_of_photos>20 and (num_of_photos/num_of_days)>10:
            #if num_of_photos>20 and (num_of_photos/num_of_days)>10 and _get_geo_distribution(clusters[c])>50.0:
                print c, num_of_days, num_of_photos, _get_geo_distribution(clusters[c]) 
                trip, crtd      =   Trip.objects.get_or_create(
                                        flickr_account=self, 
                                        start_date=start_date.replace(tzinfo=utc), 
                                        end_date=end_date.replace(tzinfo=utc), 
                                        num_of_days=num_of_days,
                                        num_of_photos=num_of_photos
                                    )
                FlickrAccountPhoto.objects.filter(id__in=[p['id'] for p in clusters[c]]).update(trip=trip)
                #self.update_moment(trip)
        return result

    def update_moment(self, trip, diff_in_seconds=3600):
        photos_qs       =   FlickrAccountPhoto.objects.filter(flickrac=self, trip=trip).filter(~Q(datetaken=None)).order_by('datetaken')
        photos_list     =   photos_qs.values('id', 'title', 'tags', 'views', 'latitude', 'longitude', 'place_id', 'woeid', 'photoid', 'datetaken')
        moment_clstrs   =   _get_moment_cluster(photos_list, diff_in_seconds)
        moment_clstrs_list  =   []
        for mc in moment_clstrs:
            moment_dict     =   {}
            moment_dict['num_of_photos']    =   len(moment_clstrs[mc])
            moment_dict['start_date']       =   min([j['datetaken'] for j in moment_clstrs[mc]])
            moment_dict['end_date']         =   max([j['datetaken'] for j in moment_clstrs[mc]])
            moment_dict['duration']         =   math.ceil((moment_dict['end_date'] - moment_dict['start_date']).total_seconds()/(60*60))
            moment_dict['title']            =   None
            moment_dict['photos']           =   [p['id'] for p in moment_clstrs[mc]]
            moment_clstrs_list.append(moment_dict)
        trip.moment_cluster =   json.dumps(moment_clstrs_list, cls=DjangoJSONEncoder)
        trip.save()

    def suggested_trips(self, forced=False):
        all_trip    =   []
        if not self.synced:
            self.sync_flickr()
        trips   =   Trip.objects.filter(flickr_account=self).order_by('-num_of_days')
        if len(trips)==0:
            self.construct_trip()
            trips   =   Trip.objects.filter(flickr_account=self).order_by('-num_of_days')
        for trip in trips:
            #self.update_moment(trip)
            all_trip.append(trip.get_trip_json())
        return all_trip


class Trip(models.Model):
    flickr_account  =   models.ForeignKey(FlickrAccountSync)
    title           =   models.CharField(max_length=255, blank=True, null=True)
    start_date      =   models.DateTimeField(blank=True, null=True)
    end_date        =   models.DateTimeField(blank=True, null=True)
    num_of_days     =   models.PositiveIntegerField(default=1)
    num_of_photos   =   models.PositiveIntegerField(default=0)
    status          =   models.CharField(max_length=255, default='complete')
    saved_trip      =   models.TextField(blank=True, null=True)
    moment_cluster  =   models.TextField(blank=True, null=True)
    serai_tripid    =   models.CharField(max_length=255, blank=True, null=True)

    def get_trip_json(self):
        if not self.serai_tripid:
            trip_dict       =   model_to_dict(self, fields=[], exclude=['flickr_account', 'status', 'saved_trip'])
            moment_cluster  =   json.loads(trip_dict.pop('moment_cluster'))
            photo_ids       =   [p for clster in moment_cluster for p in clster['photos']]
            photos_list     =   FlickrAccountPhoto.objects.filter(id__in=photo_ids).values()
            photos_dict     =   {}
            for ph in photos_list:
                sizes = ph.pop('sizes')
                ph.update(json.loads(sizes))
                ph_pk = ph['id']
                ph['id'] = ph['photoid']
                photos_dict[ph_pk] = ph
            for mc in moment_cluster:
                mc['photos'] = [photos_dict[p] for p in mc['photos']]
            trip_dict['moments']  = moment_cluster
            return trip_dict
        else:
            return json.loads(self.serai_tripid)


class FlickrAccountPhoto(models.Model):
    flickrac        =   models.ForeignKey(FlickrAccountSync)
    photoid         =   models.CharField(max_length=255)
    ispublic        =   models.PositiveIntegerField(default=1)
    machine_tags    =   models.TextField(blank=True, null=True)
    owner           =   models.CharField(max_length=255, blank=True, null=True)
    title           =   models.CharField(max_length=255, blank=True, null=True)
    tags            =   models.TextField(blank=True, null=True)
    datetaken       =   models.CharField(max_length=255, blank=True, null=True)
    isfriend        =   models.PositiveIntegerField(default=0)
    secret          =   models.CharField(max_length=255, blank=True, null=True)
    accuracy        =   models.PositiveIntegerField(default=0)
    isfamily        =   models.PositiveIntegerField(default=0)
    views           =   models.PositiveIntegerField(default=0)
    farm            =   models.CharField(max_length=255, blank=True, null=True)
    datetakengranularity    =   models.CharField(max_length=255, blank=True, null=True)
    latitude        =   models.CharField(max_length=255, blank=True, null=True)
    longitude       =   models.CharField(max_length=255, blank=True, null=True)
    server          =   models.CharField(max_length=255, blank=True, null=True)
    context         =   models.CharField(max_length=255, blank=True, null=True)
    geo_is_public   =   models.PositiveIntegerField(default=0)
    geo_is_friend   =   models.PositiveIntegerField(default=0)
    geo_is_contact  =   models.PositiveIntegerField(default=0)
    geo_is_family   =   models.PositiveIntegerField(default=0)
    place_id        =   models.CharField(max_length=255, blank=True, null=True)
    woeid           =   models.CharField(max_length=255, blank=True, null=True)
    sizes           =   models.TextField(blank=True, null=True)
    trip            =   models.ForeignKey(Trip, blank=True, null=True)

    def get_json(self):
        pass

class ServicesUsed(models.Model):
    email       =   models.CharField(max_length=255, blank=True, null=True)
    services    =   models.TextField(blank=True, null=True)

class Feedback(models.Model):
    mesg        =   models.TextField(blank=True, null=True)


class Geoname(models.Model):
    geonameid = models.IntegerField(primary_key=True)
    name = models.CharField(max_length=200L, blank=True)
    asciiname = models.CharField(max_length=200L, blank=True)
    alternatenames = models.CharField(max_length=4000L, blank=True)
    latitude = models.DecimalField(null=True, max_digits=12, decimal_places=7, blank=True)
    longitude = models.DecimalField(null=True, max_digits=12, decimal_places=7, blank=True)
    fclass = models.CharField(max_length=1L, blank=True)
    fcode = models.CharField(max_length=10L, blank=True)
    country = models.CharField(max_length=2L, blank=True)
    cc2 = models.CharField(max_length=60L, blank=True)
    admin1 = models.CharField(max_length=20L, blank=True)
    admin2 = models.CharField(max_length=80L, blank=True)
    admin3 = models.CharField(max_length=20L, blank=True)
    admin4 = models.CharField(max_length=20L, blank=True)
    population = models.IntegerField(null=True, blank=True)
    elevation = models.IntegerField(null=True, blank=True)
    gtopo30 = models.IntegerField(null=True, blank=True)
    timezone = models.CharField(max_length=40L, blank=True)
    moddate = models.DateField(null=True, blank=True)
    geohash = models.CharField(max_length=255, null=True, blank=True)
    class Meta:
        db_table = 'geoname'


class Cities(models.Model):
    city        =   models.CharField(max_length=80L)
    country     =   models.CharField(max_length=2L)
    region_code =   models.CharField(max_length=2L, blank=True)
    population  =   models.IntegerField(null=True, blank=True)
    lat         =   models.FloatField(null=True, blank=True)
    lon         =   models.FloatField(null=True, blank=True)
    last_word   =   models.CharField(max_length=32L, blank=True)
    geohash     =   models.CharField(max_length=255, null=True, blank=True)
    class Meta:
        db_table = 'cities'


class Countries(models.Model):
    country         =   models.CharField(max_length=64L, primary_key=True)
    country_code    =   models.CharField(max_length=2L, blank=True)
    lat             =   models.FloatField(null=True, blank=True)
    lon             =   models.FloatField(null=True, blank=True)
    last_word       =   models.CharField(max_length=32L, blank=True)
    class Meta:
        db_table = 'countries'

