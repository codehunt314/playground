import os
from oauth2client import xsrfutil
from oauth2client.client import flow_from_clientsecrets
from oauth2client.django_orm import Storage
from oauth2client.client import OAuth2Credentials
from stockphotos.local_settings import flickr_api_key, flickr_api_secret


req_token_url           =   "http://www.flickr.com/services/oauth/request_token"
authorize_url           =   "http://www.flickr.com/services/oauth/authorize"
access_token_url        =   "http://www.flickr.com/services/oauth/access_token"

google_plus_api_key     =   "232715324309-atelahmnesduuub6a7nrd005c7n5jtnq.apps.googleusercontent.com"
google_plus_api_secret  =   "owPMG2CvigHGGjx9va_k47My"

CLIENT_SECRETS = os.path.join(os.path.dirname(__file__), 'client_secrets.json')

FLOW    =   flow_from_clientsecrets(
                CLIENT_SECRETS,
                scope='https://www.googleapis.com/auth/plus.me https://picasaweb.google.com/data/',
                redirect_uri='http://localhost:8000/playground/google/oauth2callback'
            )
